package com.tutkal.spider.Fragment.CustomerInformation;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.tutkal.spider.Activity.PaymentHistoryActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewAdapter;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.FragmentInformationFinancialBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.database.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FinancialFragment extends Fragment {

    public FinancialFragment() {
        // Required empty public constructor
        instance=this;
    }


    private static FinancialFragment instance;
    public static FinancialFragment getInstance() {
        return instance;
    }
    private FragmentInformationFinancialBinding binding;
    private Customer customer=new Customer();
    private ArrayList<RecyclerClass> arrayList=new ArrayList<>();
    private ArrayList<String> arrayOdemeler=new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view= inflater.inflate(R.layout.fragment_information_financial, container, false);
        binding=FragmentInformationFinancialBinding.inflate(inflater,container,false);
        customer.setCustomerID(getSharedCustomerID());
        //Ezrak.getInstance().getCustomer().CustomerInformationFinancial(customer,binding);
        Toast.makeText(getContext()     , "asdasdasd", Toast.LENGTH_SHORT).show();
        final RecyclerViews k=Red.Instance.getView().GetRecyclerViews();
        Alagan.Instance.dbString
                .put("command","payNames")
                .put("customerID",String.valueOf(customer.getCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("taksitle ödeme",response);
                        try {
                            JSONArray array=new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object=array.getJSONObject(i);
                                arrayOdemeler.add(object.getString("debt_name"));
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,arrayOdemeler);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            binding.spinner3.setAdapter(aa);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
        binding.FragmentCustomerInformationFinancialOdemeGecmisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),PaymentHistoryActivity.class);
                startActivity(intent);
            }
        });
        binding.button16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //binding.setCustomerInformation(customer);
        return binding.getRoot();
    }

    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(getContext(), "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }
    public Customer getCustomer(){
        return customer;
    }

    public boolean getPaymentMethod(){
        String t=binding.textView44.getText().toString();
        if (t.equals("Peşin Ödeme")) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //Ezrak.getInstance().getCustomer().CustomerInformationFinancial(customer,binding);
    }
}

