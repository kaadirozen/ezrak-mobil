package com.tutkal.spider.Fragment.CustomerInformation;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.tutkal.spider.Activity.Customer.CustomerInformationActivitiy;
import com.tutkal.spider.Activity.OldRepairActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.FragmentInformationDeviceInfarmationBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateDeviceBinding;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.Customer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class DeviceInfarmationFragment extends Fragment {

    public DeviceInfarmationFragment() {
        // Required empty public constructor
        instance=this;
    }
    private static DeviceInfarmationFragment instance;
    public static DeviceInfarmationFragment getInstance() {
        return instance;
    }

    private FragmentInformationDeviceInfarmationBinding binding;
    private Customer customer=new Customer();
    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view= inflater.inflate(R.layout.fragment_information_device_infarmation, container, false);
        binding=FragmentInformationDeviceInfarmationBinding.inflate(inflater,container,false);

        customer.setCustomerID(getSharedCustomerID());
        Ezrak.getInstance().getCustomer().CustomerInformationDevice(customer,binding);
        bakimYap();
        binding.FragmentInformationDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),OldRepairActivity.class));
            }
        });
        binding.setCustomerInformation(customer);
        return binding.getRoot();
    }
    private void bakimYap() {
        binding.button15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupCustomerUpdateDeviceBinding binding= DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                        R.layout.popup_customer_update_device,null,false);
                final android.app.AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding()
                        .PopupCusstomerInformationDeviceUpdate(getContext(),binding);
                final Customer customer=DeviceInfarmationFragment.getInstance().getCustomer();

                FragmentInformationDeviceInfarmationBinding binding2=DeviceInfarmationFragment.getInstance().binding();
                binding.textView161.setVisibility(View.GONE);
                binding.editText32.setVisibility(View.GONE);
                binding.textView166.setVisibility(View.GONE);
                binding.editText34.setVisibility(View.GONE);
                binding.editText35.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DatePickerDialog(getContext(),date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });
                tarihIslemleri(binding);
                binding.PopupCustomerUpdateDeviceTextViewEdit.setText("Bakım Yap");
                binding.PopupCustomerUpdateDeviceCheckBoxBlokKarbo.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxBlokKarbon.isChecked());
                binding.PopupCustomerUpdateDeviceCheckBoxKarbonFiltre.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxKarbonFiltre.isChecked());
                binding.PopupCustomerUpdateDeviceCheckBoxMebrahFiltre.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxMebrahFiltre.isChecked());
                binding.PopupCustomerUpdateDeviceCheckBoxSonKarbon.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxSonKarbon.isChecked());
                binding.PopupCustomerUpdateDeviceCheckBoxMikron5.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxMikron5.isChecked());
                customer.setCustomerID(getSharedCustomerID());
                binding.PopupCustomerUpdateDeviceTextViewEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        //Ezrak.getInstance().getCustomer().CustomerUpdateDevice(customer);
                        Red.Instance.getView().AlertDiyalogTrueFalse(getContext(), "Müşteri Düzenleme",
                                "Müşteriyi Düzenlemek İstediğinize Emin misiniz?", new IEzrak() {
                                    @Override
                                    public void callback(Object data) {

                                        if((boolean)data){
                                            customer.setBlokKarbon(binding.PopupCustomerUpdateDeviceCheckBoxBlokKarbo.isChecked());
                                            customer.setKarbonFiltre(binding.PopupCustomerUpdateDeviceCheckBoxKarbonFiltre.isChecked());
                                            customer.setMebrahFiltre(binding.PopupCustomerUpdateDeviceCheckBoxMebrahFiltre.isChecked());
                                            customer.setSonKarbon(binding.PopupCustomerUpdateDeviceCheckBoxSonKarbon.isChecked());
                                            customer.setMikron5(binding.PopupCustomerUpdateDeviceCheckBoxMikron5.isChecked());
                                            //Ezrak.getInstance().getCustomer().CustomerUpdateDevice(customer,binding);
                                            Ezrak.getInstance().getCustomer().BakimYap(customer,binding);
                                            startActivity(new Intent(getContext(),CustomerInformationActivitiy.class));
                                            getActivity().finish();
                                        }
                                    }
                                });
                    }
                });
                binding.PopupCustomerUpdateDeviceTextViewExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                binding.setCustomerUpdate(customer);
            }
        });
    }
    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(getContext(), "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }
    private void tarihIslemleri(final PopupCustomerUpdateDeviceBinding bindingg) {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                bindingg.editText35.setText(sdf.format(myCalendar.getTime()));
            }
        };

    }
    private boolean getCheckBox(String s) {
        if (s.equals("1")) {
            return true;
        } else {
            return false;
        }
    }
    public Customer getCustomer(){
        return customer;
    }
    public FragmentInformationDeviceInfarmationBinding binding(){
        return binding;
    }

}
