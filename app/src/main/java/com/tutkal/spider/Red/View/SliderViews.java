package com.tutkal.spider.Red.View;

import android.content.Context;
import android.graphics.Color;

import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.SliderAdapter;
import com.tutkal.spider.Red.Class.RecyclerClass;

import java.util.ArrayList;

public class SliderViews {
    public SliderViews() {
    }

    public  SliderAdapter SliderAdapter(Context contextContext, SliderView sliderView, ArrayList<RecyclerClass> arrayList, int i){
        SliderAdapter adapter = new SliderAdapter(contextContext, R.layout.item_main_card,arrayList,i);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        //sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.rgb(248,131,62));
        sliderView.setIndicatorUnselectedColor(Color.rgb(200,200,200));
        sliderView.setScrollTimeInSec(1000); //set scroll delay in seconds :
        sliderView.startAutoCycle();

        return adapter;
    }
}
