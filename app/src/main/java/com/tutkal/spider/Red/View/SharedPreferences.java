package com.tutkal.spider.Red.View;

import android.content.Context;
import android.preference.PreferenceManager;

public class SharedPreferences {

    private Context mContext;

    public SharedPreferences(Context context) {
        this.mContext = context;
    }

    public  void SharedSet(Context context, String SharedID, String value){
        android.content.SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SharedID, value);
        editor.commit();
    }

    public  void SharedGet(Context context, String SharedID , RedSharedListener listener){
        String value;
        android.content.SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        value = preferences.getString(SharedID, "-1");
        listener.onResponse(value);
    }

    public  void SharedRemove(Context context, String SharedID){
        android.content.SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.remove(SharedID);
        editor.commit();
    }

    public interface RedSharedListener{
        void onResponse(String id);
    }

}
