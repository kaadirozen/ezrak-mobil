package com.tutkal.spider;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tutkal.spider.Activity.Customer.CustomerInformationActivitiy;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.Activity.MainActivity;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class deneme extends AppCompatActivity {

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheetView;
    private ArrayList<RecyclerClass> arrayListe=new ArrayList<RecyclerClass>();
    private RecyclerView mRecyclerView;
    //private ArrayList<RecyclerClass> arrayListNamePhoneFilter,arrayListBolgeFilter,arrayListAlfabetikSıralama,arrayListGunSıralama,arrayListTumListe;
    private ArrayList<RecyclerClass> arrayListNamePhoneFilter,arrayListBolgeFilter,arrayListAlfabetikSıralama,arrayListGunSıralama,arrayListTumListe,arrayfilter;
    private SearchView edtNamePhoneSearch;
    private EditText edtBolge;
    private Spinner spinner;
    private RadioGroup radioGroup;
    private RadioButton radioBtnAlfabetik,radioBtnGun,radioBtnBakimGunu;
    private ProgressBar progressBar;
    RecyclerViews k ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
       /* String[] axisData = new String[12];
        int[] yAxisData = new int[12];
        for (int i = 0; i <12 ; i++) {

            axisData[i]="ocak";
            yAxisData[i]=i*20;

        }
        istatistik(axisData,yAxisData);*/
        new Red(getApplicationContext());
        k=Red.Instance.getView().GetRecyclerViews();
        init();
        getCustomerList();
        //FilteredSıralama();
        //FilteredSıralama();


        edtNamePhoneSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                //FilteredNamePhone(s);
                return false;
            }
        });
        edtBolge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable text) {

                // FilteredBolge(text.toString());

            }
        });
        //  FilteredSıralama();
        // FilteredNamePhone(edtNamePhoneSearch.getQuery().toString());


    }

    private void istatistik(String[] axisData, int[] yAxisData) {
        LineChartView lineChartView;

        lineChartView = findViewById(R.id.chart2);
        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();


        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));


        for (int i = 0; i < axisData.length; i++) {
            axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
        }

        for (int i = 0; i < yAxisData.length; i++) {
            yAxisValues.add(new PointValue(i, yAxisData[i]));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);
        line.setColor(Color.parseColor("#f8833e"));
        Axis axis = new Axis();

        axis.setTextSize(11);
        axis.setTextColor(Color.parseColor("#f8833e"));
        axis.setValues(axisValues);


        data.setAxisXBottom(axis);

        Axis yAxis = new Axis();

        yAxis.setTextColor(Color.parseColor("#999797"));


        data.setAxisYLeft(yAxis);


        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = 1000;

        lineChartView.setLineChartData(data);
        Viewport viewport2 = new Viewport(lineChartView.getMaximumViewport());

        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport2);
    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        progressBar=findViewById(R.id.progressBar2);
        mRecyclerView=findViewById(R.id.ActivityCustomerList_RecyclerView);
        bottomSheetView = getLayoutInflater().inflate(R.layout.popup_customer_list_filter, null);
        bottomSheetDialog = new BottomSheetDialog(deneme.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);
        edtNamePhoneSearch=findViewById(R.id.searchView);
        edtBolge=bottomSheetDialog.findViewById(R.id.editText18);
        spinner=bottomSheetDialog.findViewById(R.id.spinner2);

        Red.Instance.getView().Spinner(spinner,"İl","İlçe","Mahalle");

        radioGroup=bottomSheetDialog.findViewById(R.id.radioGroup3);
        radioBtnAlfabetik=bottomSheetDialog.findViewById(R.id.radioButton7);
        radioBtnGun=bottomSheetDialog.findViewById(R.id.radioButton5);
        radioBtnBakimGunu=bottomSheetDialog.findViewById(R.id.radioButton8);
        arrayListTumListe=new ArrayList<>();
    }

    private ArrayList<RecyclerClass> FilteredBolge(String text, ArrayList<RecyclerClass> arrayListe, ArrayList<RecyclerClass> liste) {
        arrayListBolgeFilter=new ArrayList<>();
        switch (spinner.getSelectedItemPosition()){
            case 0:
                //il

                for(RecyclerClass item :FilteredSıralama()){
                    if(item.getAs()[4].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 1:
                //ilçe

                for(RecyclerClass item : FilteredSıralama()){
                    if(item.getAs()[5].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 2:
                //mahalle

                for(RecyclerClass item :FilteredSıralama()){
                    if(item.getAs()[6].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
        }
        k.recyclerViewAdapter.filterList(arrayListBolgeFilter);

        return arrayListBolgeFilter;
    }

    private ArrayList<RecyclerClass> FilteredSıralama(){
        /*
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                arrayListTumListe.clear();
                switch (radioGroup.getCheckedRadioButtonId()){
                    //alfabetik
                    case R.id.radioButton7:
                        Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {

                                Collections.sort((ArrayList)data,RecyclerClass.StuNameComparator);
                                k.recyclerViewAdapter.filterList((ArrayList)data);
                                arrayListTumListe=(ArrayList)data;

                                //FilteredBolge(edtBolge.getText().toString());


                            }
                        });

                        break;

                    //gun
                    case R.id.radioButton5:
                        //k.recyclerViewAdapter.filterList(arrayListGunSıralama);
                        arrayListTumListe=Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {

                                k.recyclerViewAdapter.filterList((ArrayList)data);
                                //FilteredBolge(edtBolge.getText().toString());

                            }
                        });

                        //bakim Günü
                        break;
                    case R.id.radioButton8:

                        Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                ArrayList<RecyclerClass> a=new ArrayList<>();
                                a=(ArrayList)data;
                                for(RecyclerClass item : a ){
                                    if(item.getAs()[2].equals("0")){
                                        arrayListTumListe.add(item);
                                        Log.e("asdasdasdasd",item.getAs()[1]);
                                    }

                                    k.recyclerViewAdapter.filterList(arrayListTumListe);
                                    //  FilteredBolge(edtBolge.getText().toString());

                                }
                            }
                        });




                        break;
                }
            }
        });
*/




        return arrayListTumListe;
    }

    private  ArrayList<RecyclerClass> FilteredNamePhone(String s ) {
        arrayListNamePhoneFilter=new ArrayList<>();
        for(RecyclerClass item : FilteredBolge(edtBolge.getText().toString(), arrayListe, arrayListe)){
            if(item.getAs()[0].toLowerCase().contains(s.toLowerCase())||item.getAs()[1].toLowerCase().contains(s.toLowerCase())){
                arrayListNamePhoneFilter.add(item);
            }
        }
        k.recyclerViewAdapter.filterList(arrayListNamePhoneFilter);
        return arrayListNamePhoneFilter;
    }


    private void getCustomerList(){
        // Log.e("getcustomer","asdasd");
        arrayListAlfabetikSıralama=new ArrayList<>();
        arrayListGunSıralama=new ArrayList<>();

        Alagan.Instance.dbString.put("command","customerList")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("subcommand","kalangun")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                //arrayListe.add(new RecyclerClass(5,"Furkan Bahadır Özdemir","05061043257","224","TL"));
                                arrayListe.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("phone"),objs.getString("kalangun"),"Gün"

                                        , objs.getString("province"),objs.getString("district"),objs.getString("neighborhood"),objs.getString("address")
                                ));
                            }
                            arrayfilter=arrayListe;

                            // Log.e( "onResponse1: ",arrayfilter.get(0).getAs()[0] );
                            k.recyclerViewAdapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                            editTextNamePhoneSerach();
                        }catch (Exception e){

                        }
                    }
                });



        k.addItem(5,R.id.Item_CustomerList_Name,R.id.Item_CustomerList_Phone,R.id.Item_CustomerList_Day,R.id.textView155,R.id.Item_CustomerList_Counter)
                .RecyclerView(mRecyclerView,arrayListe,R.layout.item_customer_list,new RecyclerViewOnClick() {
                    @Override
                    public void OnClick(int ID) {
                        Red.Instance.getShared().SharedSet(deneme.this,"customerID",String.valueOf(ID));
                        startActivity(new Intent(getApplicationContext(), CustomerInformationActivitiy.class));

                    }
                }).Creat();
    }

    public void ActivityListFilter(View view) {
        switch (view.getId()){
            case R.id.Activity_CustomerList_ImageBack:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
            case R.id.Activity_CustomerList_ImageFilter:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetDialog.show();
                break;
        }
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(deneme.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }



    @Override
    protected void onResume() {
        super.onResume();

        if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton7){
            Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                @Override
                public void callback(Object data) {


                    Collections.sort((ArrayList)data,RecyclerClass.StuNameComparator);

                    k.recyclerViewAdapter.filterList((ArrayList)data);
                    arrayListTumListe=(ArrayList)data;
                    FilteredNamePhone(edtBolge.getText().toString());
                }
            });
        }
        if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton5){
            arrayListTumListe=Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                @Override
                public void callback(Object data) {

                    k.recyclerViewAdapter.filterList((ArrayList)data);
                    FilteredNamePhone(edtBolge.getText().toString());
                }
            });

        }
        if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton8){
            arrayListTumListe.clear();
            Ezrak.getInstance().getCustomerList().getCustomerList(k, getSharedUserID(), new IEzrak() {
                @Override
                public void callback(Object data) {
                    ArrayList<RecyclerClass> a=new ArrayList<>();
                    a=(ArrayList)data;
                    for(RecyclerClass item : a ){
                        if(item.getAs()[2].equals("0")){
                            arrayListTumListe.add(item);
                            Log.e("asdasdasdasd",item.getAs()[1]);
                        }
                        k.recyclerViewAdapter.filterList(arrayListTumListe);
                        FilteredNamePhone(edtBolge.getText().toString());
                    }
                }
            });

        }
    }
    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //http://android-er.blogspot.com/2016/06/bottomsheetdialog-example.html
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };

    public void editTextNamePhoneSerach(){
        edtNamePhoneSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                FilteredNamePhone(s);
                return false;
            }
        });
    }
    public void spinnerSearch(final ArrayList<RecyclerClass> arrayListe, final ArrayList<RecyclerClass> arrayfilter){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                FilteredBolge(edtBolge.getText().toString(),arrayListe,arrayfilter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


}
