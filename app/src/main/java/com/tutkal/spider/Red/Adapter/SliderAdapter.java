package com.tutkal.spider.Red.Adapter;

import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.Activity.LastAddedCustomerActivity;
import com.tutkal.spider.Activity.LatestRegulationsActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Class.RecyclerClass;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {


    private int layout;

    private Context context;
    private int activityNo;
    private ArrayList<RecyclerClass> arrayList;
    public SliderAdapter(Context context, int layoutID, ArrayList<RecyclerClass> arrayList, int activityNo) {
        this.layout=layoutID;
        this.context = context;
        this.activityNo=activityNo;
        this.arrayList=arrayList;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(layout, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        RecyclerClass sliderClass=arrayList.get(position);

        viewHolder.name.setText(sliderClass.getAs()[0]);
        viewHolder.degisken.setText(sliderClass.getAs()[1]);
        viewHolder.counter.setText("" +(position+1));
        ((ConstraintLayout)viewHolder.name.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (activityNo){
                    case 1:
                        Intent intent=new Intent(context, LastAddedCustomerActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
                    case 2:
                        Intent intent2=new Intent(context, CustomerListActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent2);

                        break;
                    case 3:
                        Intent intent3=new Intent(context, LatestRegulationsActivity.class);
                        intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent3);
                        break;
                    default:
                        break;
                }
            }
        });


    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return arrayList.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;

        TextView name,degisken,counter;


        public SliderAdapterVH(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.textView74);
            degisken = itemView.findViewById(R.id.textView76);
            counter = itemView.findViewById(R.id.textView75);

            this.itemView = itemView;
        }
    }
}