package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.tutkal.spider.R;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.ActivityProfilBinding;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.User;

public class ProfilActivity extends AppCompatActivity {

    private ActivityProfilBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profil);
        binding= DataBindingUtil.setContentView(ProfilActivity.this,R.layout.activity_profil);
        init();
        User user=new User();
        user.setUserID(getSharedUserID());
        Ezrak.getInstance().getUser(user).UserProfilDetail(binding);
        binding.ActivityProfilImageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfilActivity.this,MainActivity.class));
                finish();
            }
        });
        binding.ActivityProfilImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfilActivity.this,ProfilUpdateActivity.class));
                finish();
            }
        });
        binding.setProfil(user);
        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Red.Instance.getView().AlertDiyalogTrueFalse(ProfilActivity.this,
                        "Çıkış", "Çıkış yapmak istediğinize eminmisiniz ?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Red.Instance.getShared().SharedRemove(getApplicationContext(),"userID");
                                    Red.Instance.getShared().SharedRemove(getApplicationContext(),"customerID");
                                    startActivity(new Intent(ProfilActivity.this,LoginActivity.class));
                                    finish();

                                }
                            }
                        });
            }
        });
    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(ProfilActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
               userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        User user=new User();
        user.setUserID(getSharedUserID());
        Ezrak.getInstance().getUser(user).UserProfilDetail(binding);
    }
}
