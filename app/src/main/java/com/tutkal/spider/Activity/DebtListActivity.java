
package com.tutkal.spider.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tutkal.spider.Activity.Customer.CustomerInformationActivitiy;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DebtListActivity extends AppCompatActivity {


    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheetView;

    private RecyclerView mRecyclerView;
    final    RecyclerViews k = Red.Instance.getView().GetRecyclerViews();
    private SearchView edtNamePhoneSearch;
    private EditText edtBolge;
    private Spinner spinner;
    private ArrayList<RecyclerClass> arrayfilterListe=new ArrayList<>();
    private ArrayList<RecyclerClass> arrayfilterListe2=new ArrayList<>();
    private ArrayList<RecyclerClass> arrayTumListe=new ArrayList<RecyclerClass>();
    private RadioGroup radioGroup;
    private RadioButton radioBtnAlfabetik,radioBtnGun,radioBtnBakimGunu;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_list);
        init();
        getCustomerList();



        edtNamePhoneSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                FilteredNamePhone(s,FilteredBolge(edtBolge.getText().toString(),arrayfilterListe));

                return false;
            }
        });
        edtBolge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable text) {

                FilteredBolge(text.toString(), FilteredNamePhone(edtNamePhoneSearch.getQuery().toString(),arrayfilterListe));


            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                FilteredBolge(edtBolge.getText().toString(), FilteredNamePhone(edtNamePhoneSearch.getQuery().toString(),arrayfilterListe));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()){
                    //taksitli
                    case R.id.radioButton7:
                        arrayfilterListe.clear();
                        for(RecyclerClass item : arrayfilterListe2 ){
                            if(item.getAs()[8].equals("taksit")){
                                arrayfilterListe.add(item);
                                Log.d("taksit",item.getAs()[8]);
                            }
                            k.recyclerViewAdapter.filterList(arrayfilterListe);
                        }
                        break;
                    //peşin
                    case R.id.radioButton5:
                        //k.recyclerViewAdapter.filterList(arrayListGunSıralama);
                       arrayfilterListe.clear();
                        for(RecyclerClass item : arrayfilterListe2 ) {
                            if (item.getAs()[8].equals("peşin")) {
                                arrayfilterListe.add(item);
                                Log.d("peşin", item.getAs()[8]);
                            }
                            k.recyclerViewAdapter.filterList(arrayfilterListe);
                        }//bakim Günü
                        break;
                    case R.id.radioButton8:

                        arrayfilterListe.clear();
                        for(RecyclerClass item : arrayfilterListe2 ){
                            if(item.getAs()[8].equals("taksit_gecen")){
                                arrayfilterListe.add(item);
                                Log.e("taksit_geçen",item.getAs()[8]);
                            }
                            k.recyclerViewAdapter.filterList(arrayfilterListe);

                        }

                        break;
                }

            }
        });








    }




    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        mRecyclerView=findViewById(R.id.dfsfdfsf);
        edtNamePhoneSearch=findViewById(R.id.searchView2);
        progressBar=findViewById(R.id.progressBar);

        bottomSheetView = getLayoutInflater().inflate(R.layout.popup_customer_list_filter, null);
        bottomSheetDialog = new BottomSheetDialog(DebtListActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        edtBolge=bottomSheetDialog.findViewById(R.id.editText18);
        spinner=bottomSheetDialog.findViewById(R.id.spinner2);

        Red.Instance.getView().Spinner(spinner,"İl","İlçe","Mahalle");

        radioGroup=bottomSheetDialog.findViewById(R.id.radioGroup3);
        radioBtnAlfabetik=bottomSheetDialog.findViewById(R.id.radioButton7);
        radioBtnGun=bottomSheetDialog.findViewById(R.id.radioButton5);
        radioBtnBakimGunu=bottomSheetDialog.findViewById(R.id.radioButton8);

        radioBtnAlfabetik.setText("Taksitli");
        radioBtnGun.setText("Peşin");
        radioBtnBakimGunu.setText("Taksit zamanını geçirenler");



    }

    private void getCustomerList() {

        Alagan.Instance.dbString.put("command","customerList")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("subcommand","borc")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            progressBar.setVisibility(View.GONE);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                arrayTumListe.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("phone"),objs.getString("borc"),"TL"
                                        , objs.getString("province"),objs.getString("district"),objs.getString("neighborhood"),objs.getString("address"),objs.getString("type")
                                ));
                            }
                            Collections.sort(arrayTumListe, new Comparator<RecyclerClass>() {
                                @Override
                                public int compare(RecyclerClass recyclerClass, RecyclerClass t1) {
                                    Double distance = Double.valueOf(recyclerClass.getAs()[2]);
                                    Double distance1 = Double.valueOf(t1.getAs()[2]);
                                    if (distance.compareTo(distance1) < 0) {
                                        return 1;
                                    } else if (distance.compareTo(distance1) > 0) {
                                        return -1;
                                    } else {
                                        return 0;
                                    }
                                }
                            });
                            k.recyclerViewAdapter.notifyDataSetChanged();
                            for(RecyclerClass r : arrayTumListe){

                                arrayfilterListe.add(r);
                            }
                            for(RecyclerClass r : arrayTumListe){
                                arrayfilterListe2.add(r);
                            }

                            arrayfilterListe.clear();
                            for(RecyclerClass item : arrayfilterListe2 ) {
                                if (item.getAs()[8].equals("peşin")) {
                                    arrayfilterListe.add(item);
                                }
                                k.recyclerViewAdapter.filterList(arrayfilterListe);
                            }
                        }catch (Exception e){

                        }
                    }
                });

        k.addItem(5,R.id.Item_CustomerList_Name,R.id.Item_CustomerList_Phone,R.id.Item_CustomerList_Day,R.id.textView155,R.id.Item_CustomerList_Counter)
                .RecyclerView(mRecyclerView,arrayTumListe,R.layout.item_customer_list,new RecyclerViewOnClick() {
                    @Override
                    public void OnClick(int ID) {
                        Red.Instance.getShared().SharedSet(DebtListActivity.this,"customerID",String.valueOf(ID));
                        startActivity(new Intent(getApplicationContext(), CustomerInformationActivitiy.class));

                    }
                }).Creat();

    }
    private ArrayList<RecyclerClass> FilteredNamePhone(String s, ArrayList<RecyclerClass> arrayTumListe) {
        ArrayList<RecyclerClass> arrayListNamePhoneFilter=new ArrayList<>();
        for(RecyclerClass item :arrayTumListe ){
            if(item.getAs()[0].toLowerCase().contains(s.toLowerCase())||item.getAs()[1].toLowerCase().contains(s.toLowerCase())){
                arrayListNamePhoneFilter.add(item);
            }
        }
        k.recyclerViewAdapter.filterList(arrayListNamePhoneFilter);
        return arrayListNamePhoneFilter;
    }
    private ArrayList<RecyclerClass> FilteredBolge(String text, ArrayList<RecyclerClass> liste) {
        ArrayList<RecyclerClass> arrayListBolgeFilter=new ArrayList<>();
        switch (spinner.getSelectedItemPosition()){
            case 0:
                //il

                for(RecyclerClass item :liste){
                    if(item.getAs()[4].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 1:
                //ilçe

                for(RecyclerClass item : liste){
                    if(item.getAs()[5].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 2:
                //mahalle

                for(RecyclerClass item :liste){
                    if(item.getAs()[6].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
        }
        k.recyclerViewAdapter.filterList(arrayListBolgeFilter);

        return arrayListBolgeFilter;
    }

  /*  private ArrayList<RecyclerClass> FilteredSıralama(){

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                arrayListTumListe.clear();
                switch (radioGroup.getCheckedRadioButtonId()){
                    //taksitli
                    case R.id.radioButton7:
                        Ezrak.getInstance().getCustomerList().getCustomerListBorc(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                ArrayList<RecyclerClass> a=new ArrayList<>();
                                a=(ArrayList)data;
                                for(RecyclerClass item : a ){
                                    if(item.getAs()[8].equals("taksit")){
                                        arrayListTumListe.add(item);
                                        Log.e("asdasdasdasd",item.getAs()[1]);
                                    }
                                    k.recyclerViewAdapter.filterList(arrayListTumListe);
                                    FilteredNamePhone(edtBolge.getText().toString());
                                }
                            }
                        });

                        break;

                    //peşin
                    case R.id.radioButton5:
                        //k.recyclerViewAdapter.filterList(arrayListGunSıralama);
                        Ezrak.getInstance().getCustomerList().getCustomerListBorc(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                ArrayList<RecyclerClass> a=new ArrayList<>();
                                a=(ArrayList)data;
                                for(RecyclerClass item : a ){
                                    if(item.getAs()[8].equals("peşin")){
                                        arrayListTumListe.add(item);
                                        Log.e("asdasdasdasd",item.getAs()[1]);
                                    }
                                    k.recyclerViewAdapter.filterList(arrayListTumListe);
                                    FilteredNamePhone(edtBolge.getText().toString());
                                }
                            }
                        });

                        //bakim Günü
                        break;
                    case R.id.radioButton8:

                        Ezrak.getInstance().getCustomerList().getCustomerListBorc(k, getSharedUserID(), new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                ArrayList<RecyclerClass> a=new ArrayList<>();
                                a=(ArrayList)data;
                                for(RecyclerClass item : a ){
                                    if(item.getAs()[8].equals("taksit_gecen")){
                                        arrayListTumListe.add(item);
                                        Log.e("asdasdasdasd",item.getAs()[1]);
                                    }
                                    k.recyclerViewAdapter.filterList(arrayListTumListe);
                                    FilteredNamePhone(edtBolge.getText().toString());
                                }
                            }
                        });




                        break;
                }
            }
        });

        return arrayListTumListe;
    }
*/



    public void ActivityDebtListImageClick(View view) {
        switch(view.getId()){
            case R.id.imageView14:
                  onBackPressed();
                finish();
            case R.id.imageView15:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetDialog.show();

        }

    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(DebtListActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }


    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //http://android-er.blogspot.com/2016/06/bottomsheetdialog-example.html
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };




}
