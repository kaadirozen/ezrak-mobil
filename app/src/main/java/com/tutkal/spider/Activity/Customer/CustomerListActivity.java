package com.tutkal.spider.Activity.Customer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;


import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tutkal.spider.Activity.MainActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;

public class CustomerListActivity extends AppCompatActivity {

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheetView;

    private RecyclerView mRecyclerView;
    private ArrayList<RecyclerClass> arrayfilterListe=new ArrayList<>();
    private ArrayList<RecyclerClass> arrayfilterListe2=new ArrayList<>();
    private ArrayList<RecyclerClass> arrayTumListe=new ArrayList<RecyclerClass>();
    private SearchView edtNamePhoneSearch;
    private EditText edtBolge;
    private Spinner spinner;
    private RadioGroup radioGroup;
    private RadioButton radioBtnAlfabetik,radioBtnGun,radioBtnBakimGunu;
    private ProgressBar progressBar;
    RecyclerViews k = Red.Instance.getView().GetRecyclerViews();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        init();
        getCustomerList();
        edtNamePhoneSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                FilteredNamePhone(s,FilteredBolge(edtBolge.getText().toString(),arrayfilterListe));

                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                FilteredBolge(edtBolge.getText().toString(), FilteredNamePhone(edtNamePhoneSearch.getQuery().toString(),arrayfilterListe));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        edtBolge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                FilteredBolge(editable.toString(), FilteredNamePhone(edtNamePhoneSearch.getQuery().toString(),arrayfilterListe));
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()){
                    //alfabetik
                    case R.id.radioButton7:
                        arrayfilterListe.clear();
                        for (RecyclerClass item3 : arrayfilterListe2) {
                            arrayfilterListe.add(item3);

                        }
                        Collections.sort(arrayfilterListe,RecyclerClass.StuNameComparator);
                        k.recyclerViewAdapter.filterList(arrayfilterListe);

                        break;

                    //gun
                    case R.id.radioButton5:
                        //k.recyclerViewAdapter.filterList(arrayListGunSıralama);
                        arrayfilterListe.clear();
                        for (RecyclerClass item3 : arrayfilterListe2) {
                            arrayfilterListe.add(item3);

                        }
                        k.recyclerViewAdapter.filterList(arrayfilterListe);

                        //bakim Günü
                        break;
                    case R.id.radioButton8:

                        arrayfilterListe.clear();
                        for(RecyclerClass item : arrayfilterListe2 ) {
                            if (item.getAs()[2].equals("0")) {
                                arrayfilterListe.add(item);
                            }
                            k.recyclerViewAdapter.filterList(arrayfilterListe);
                           /* arrayfilterListe.clear();
                            for (RecyclerClass item2 : arrayfilterListe2) {
                                arrayfilterListe.add(item2);

                                //  FilteredBolge(edtBolge.getText().toString());

                            }*/

                        }

                        break;
                }
            }
        });


    }
    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        progressBar=findViewById(R.id.progressBar2);
        mRecyclerView=findViewById(R.id.ActivityCustomerList_RecyclerView);
        bottomSheetView = getLayoutInflater().inflate(R.layout.popup_customer_list_filter, null);
        bottomSheetDialog = new BottomSheetDialog(CustomerListActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);
        edtNamePhoneSearch=findViewById(R.id.searchView);
        edtBolge=bottomSheetDialog.findViewById(R.id.editText18);
        spinner=bottomSheetDialog.findViewById(R.id.spinner2);

        Red.Instance.getView().Spinner(spinner,"İl","İlçe","Mahalle");

        radioGroup=bottomSheetDialog.findViewById(R.id.radioGroup3);
        radioBtnAlfabetik=bottomSheetDialog.findViewById(R.id.radioButton7);
        radioBtnGun=bottomSheetDialog.findViewById(R.id.radioButton5);
        radioBtnBakimGunu=bottomSheetDialog.findViewById(R.id.radioButton8);

    }
    private void getCustomerList(){
        Alagan.Instance.dbString.put("command","customerList")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("subcommand","kalangun")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("onResponse: ", response);
                            progressBar.setVisibility(View.GONE);
                            //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                //arrayListe.add(new RecyclerClass(5,"Furkan Bahadır Özdemir","05061043257","224","TL"));
                                arrayTumListe.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("phone"),objs.getString("kalangun"),"Gün"

                                        , objs.getString("province"),objs.getString("district"),objs.getString("neighborhood"),objs.getString("address")
                                ));
                            }
                            for(RecyclerClass r : arrayTumListe){
                                arrayfilterListe.add(r);
                            }
                            for(RecyclerClass r : arrayTumListe){
                                arrayfilterListe2.add(r);
                            }

                            // Log.e( "onResponse1: ",arrayfilter.get(0).getAs()[0] );
                            k.recyclerViewAdapter.notifyDataSetChanged();

                        }catch (Exception e){

                        }
                    }
                });
        initRecyclerView();



    }
    private  ArrayList<RecyclerClass> FilteredNamePhone(String s, ArrayList<RecyclerClass> arrayTumListe) {
       ArrayList<RecyclerClass> arrayListNamePhoneFilter=new ArrayList<>();
        for(RecyclerClass item :arrayTumListe ){
            if(item.getAs()[0].toLowerCase().contains(s.toLowerCase())||item.getAs()[1].toLowerCase().contains(s.toLowerCase())){
                arrayListNamePhoneFilter.add(item);
            }
        }
        k.recyclerViewAdapter.filterList(arrayListNamePhoneFilter);
        return arrayListNamePhoneFilter;
    }
    private ArrayList<RecyclerClass> FilteredBolge(String text, ArrayList<RecyclerClass> liste) {
       ArrayList<RecyclerClass> arrayListBolgeFilter=new ArrayList<>();
        switch (spinner.getSelectedItemPosition()){
            case 0:
                //il

                for(RecyclerClass item :liste){
                    if(item.getAs()[4].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 1:
                //ilçe

                for(RecyclerClass item : liste){
                    if(item.getAs()[5].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
            case 2:
                //mahalle

                for(RecyclerClass item :liste){
                    if(item.getAs()[6].toLowerCase().contains(text.toLowerCase())){
                        arrayListBolgeFilter.add(item);
                    }
                }
                break;
        }
        k.recyclerViewAdapter.filterList(arrayListBolgeFilter);

        return arrayListBolgeFilter;
    }
    private void initRecyclerView(){
        k.addItem(5,R.id.Item_CustomerList_Name,R.id.Item_CustomerList_Phone,R.id.Item_CustomerList_Day,R.id.textView155,R.id.Item_CustomerList_Counter)
                .RecyclerView(mRecyclerView,arrayTumListe,R.layout.item_customer_list,new RecyclerViewOnClick() {
                    @Override
                    public void OnClick(int ID) {
                        Red.Instance.getShared().SharedSet(CustomerListActivity.this,"customerID",String.valueOf(ID));
                        startActivity(new Intent(getApplicationContext(),CustomerInformationActivitiy.class));

                    }
                }).Creat();
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(CustomerListActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //http://android-er.blogspot.com/2016/06/bottomsheetdialog-example.html
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };
    public void ActivityListFilter(View view) {
        switch (view.getId()){
            case R.id.Activity_CustomerList_ImageBack:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
            case R.id.Activity_CustomerList_ImageFilter:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetDialog.show();
                break;
        }
    }



}


