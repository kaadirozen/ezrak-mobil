package com.tutkal.spider.ezrak.database;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomerList extends Customer {

    private String aa;

     public CustomerList(Context mContext) {
        setNumberOfInstallments(aa);
     }



     public ArrayList<RecyclerClass> getCustomerList(final RecyclerViews k, int sharedUserID, final IEzrak IEzrak){
        final ArrayList<RecyclerClass> arrayListe=new ArrayList<RecyclerClass>();
         Alagan.Instance.dbString.put("command","customerList")
                 .put("userID",String.valueOf(sharedUserID))
                 .put("subcommand","kalangun")
                 .read(new AlaganStringDatabase.AlaganListener() {
                     @Override
                     public void onResponse(String response) {
                         try {
                             //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                             JSONArray jsonArray = new JSONArray(response);
                             for (int i = 0; i<jsonArray.length();i++)
                             {
                                 JSONObject objs = jsonArray.getJSONObject(i);
                                 //arrayListe.add(new RecyclerClass(5,"Furkan Bahadır Özdemir","05061043257","224","TL"));
                                 arrayListe.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("phone"),objs.getString("kalangun"),"Gün"

                                 , objs.getString("province"),objs.getString("district"),objs.getString("neighborhood"),objs.getString("address")
                                 ));
                             }
                             IEzrak.callback(arrayListe);
                             k.recyclerViewAdapter.notifyDataSetChanged();
                         }catch (Exception e){

                         }
                     }
                 });

         return arrayListe;
     }
    public ArrayList<RecyclerClass> getCustomerListBorc(final RecyclerViews k, int sharedUserID, final IEzrak IEzrak){
        final ArrayList<RecyclerClass> arrayListe=new ArrayList<RecyclerClass>();
        Alagan.Instance.dbString.put("command","customerList")
                .put("userID",String.valueOf(sharedUserID))
                .put("subcommand","borc")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                Log.e("borc",objs.toString());
                                //arrayListe.add(new RecyclerClass(5,"Furkan Bahadır Özdemir","05061043257","224","TL"));
                                arrayListe.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("phone"),objs.getString("borc"),"TL"
                                        , objs.getString("province"),objs.getString("district"),objs.getString("neighborhood"),objs.getString("address"),objs.getString("type")
                                ));
                            }
                            IEzrak.callback(arrayListe);
                            k.recyclerViewAdapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }
                    }
                });

        return arrayListe;
    }

     public void LastAddedCustomersMain(){

     }
     public void LastAddedCustomer(){

     }

     public void LatestRegulationsMain(){


     }

    public static  void LatestRegulations  (){


    }
    public void DebtListCash(){


    }
    public String DebtList(){

        return "sada";
    }
    public void DebtListInstallment(){


    }

 }
