package com.tutkal.spider.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class IncomeExpenseStatisticsActivity extends AppCompatActivity   {

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheetView;
    private Spinner spnYil, spnHafta;
    private Spinner spnAy;
    private TextView txtYil,txtAy,txtHafta;
    private RadioGroup radioGroupGelirBakim,radioGroupYilAyGun;
    private Button btnFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_expense_statistics);

        init();

        gelir();




        radioGroupGelirBakim.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int k) {

                if(radioGroupGelirBakim.getCheckedRadioButtonId()==R.id.radioButton4){
                    //Toast.makeText(getApplicationContext(), "gelir", Toast.LENGTH_SHORT).show();
                    gelir();
                }else if(radioGroupGelirBakim.getCheckedRadioButtonId()==R.id.radioButton){
                    //Toast.makeText(getApplicationContext(), "gider", Toast.LENGTH_SHORT).show();
                    bakim();

                }
            }


        });









    }
    private void gelir() {
       final String[] axisData = new String[7];
      final   int[] yAxisData = new int[7];
        Alagan.Instance.dbString
                .put("command","homePageMobile")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e( "onResponse: ",response );

                            JSONObject objs = new JSONObject(response);

                            JSONArray jsonArray = new JSONArray(objs.getString("statics"));

                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject obje = jsonArray.getJSONObject(i);
                                axisData[i]=obje.getString("day");
                                yAxisData[i]=obje.getInt("total");
                            }




                        }catch (Exception e){

                        }
                        istatistik(axisData,yAxisData );

                    }
                });

    }
    private void bakim() {
        final String[] axisData = new String[7];
        final   int[] yAxisData = new int[7];
        Alagan.Instance.dbString
                .put("command","statisticsMaintenance")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e( "onResponse: ",response );

                            JSONObject objs = new JSONObject(response);

                            JSONArray jsonArray = new JSONArray(objs.getString("day"));

                            for (int i = 0; i <jsonArray.length() ; i++) {
                                String obje =jsonArray.getString(i);
                                axisData[i]=obje;
                            }
                            JSONArray jsonArray2 = new JSONArray(objs.getString("data"));
                            for (int i = 0; i <jsonArray2.length() ; i++) {
                                int obje =jsonArray2.getInt(i);

                                yAxisData[i]=obje;

                            }
                        }catch (Exception e){

                        }
                        istatistik2(axisData,yAxisData );

                    }
                });
    }
    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        bottomSheetView = getLayoutInflater().inflate(R.layout.popup_statistics, null);
        bottomSheetDialog = new BottomSheetDialog(IncomeExpenseStatisticsActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);
        spnYil =bottomSheetDialog.findViewById(R.id.editTextYil);
        spnHafta =bottomSheetDialog.findViewById(R.id.editTextHafta);
        spnAy=bottomSheetDialog.findViewById(R.id.editTextAy);
        btnFilter=bottomSheetDialog.findViewById(R.id.button2);

        Red.Instance.getView().Spinner(spnAy,"Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık");
        Red.Instance.getView().Spinner(spnYil,"2020","2021");
        Red.Instance.getView().Spinner(spnHafta,"1 hafta","2 hafta","3 hafta","4 hafta","5 hafta");
        txtYil=bottomSheetDialog.findViewById(R.id.textViewYil);
        txtAy=bottomSheetDialog.findViewById(R.id.textViewAy);
        txtHafta=bottomSheetDialog.findViewById(R.id.textViewHafta);
        radioGroupYilAyGun=bottomSheetDialog.findViewById(R.id.radioGroup);


        radioGroupGelirBakim=findViewById(R.id.radioGroup4);

    }
    public void ActivityStatistics(View view) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.show();


    }
    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //http://android-er.blogspot.com/2016/06/bottomsheetdialog-example.html
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };
    public void ActivityStatisticsImage(View view) {
        switch (view.getId()){
            case R.id.Activity_Statistics_ImageBack:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
            case R.id.Activity_Statistics_ImageFilter:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetDialog.show();
                break;
        }
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(getApplicationContext(), "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
    private void istatistik(String[] axisData, int[] yAxisData) {
        LineChartView lineChartView;

        lineChartView = findViewById(R.id.chart2);
        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();


        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));


        for (int i = 0; i < axisData.length; i++) {
            axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
        }

        for (int i = 0; i < yAxisData.length; i++) {
            yAxisValues.add(new PointValue(i, yAxisData[i]));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);
        line.setColor(Color.parseColor("#f8833e"));
        Axis axis = new Axis();

        axis.setTextSize(11);
        axis.setTextColor(Color.parseColor("#f8833e"));
        axis.setValues(axisValues);


        data.setAxisXBottom(axis);

        Axis yAxis = new Axis();
        yAxis.setTextSize(9);

        yAxis.setTextColor(Color.parseColor("#999797"));


        data.setAxisYLeft(yAxis);


        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = 10000;


        lineChartView.setLineChartData(data);
        Viewport viewport2 = new Viewport(lineChartView.getMaximumViewport());

        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport2);
    }
    private void istatistik2(String[] axisData, int[] yAxisData) {
        LineChartView lineChartView;

        lineChartView = findViewById(R.id.chart2);
        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();


        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));


        for (int i = 0; i < axisData.length; i++) {
            axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
        }

        for (int i = 0; i < yAxisData.length; i++) {
            yAxisValues.add(new PointValue(i, yAxisData[i]));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);
        line.setColor(Color.parseColor("#f8833e"));
        Axis axis = new Axis();

        axis.setTextSize(11);
        axis.setTextColor(Color.parseColor("#f8833e"));
        axis.setValues(axisValues);


        data.setAxisXBottom(axis);

        Axis yAxis = new Axis();
        yAxis.setTextSize(9);

        yAxis.setTextColor(Color.parseColor("#999797"));


        data.setAxisYLeft(yAxis);


        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = 500;


        lineChartView.setLineChartData(data);
        Viewport viewport2 = new Viewport(lineChartView.getMaximumViewport());

        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport2);
    }

}
