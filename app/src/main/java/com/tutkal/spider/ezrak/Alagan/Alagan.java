package com.tutkal.spider.ezrak.Alagan;

import android.content.Context;

import com.tutkal.spider.ezrak.Alagan.Class.AlaganJsonDatabase;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;


public class Alagan {
    private Context main;

    public static Alagan Instance;

    public AlaganStringDatabase dbString;
    public AlaganJsonDatabase dbJson;


    public Alagan(Context main)
    {
        this.main = main;
        Instance = this;
        dbString = new AlaganStringDatabase(main);
        dbJson = new AlaganJsonDatabase(main);

    }



}
