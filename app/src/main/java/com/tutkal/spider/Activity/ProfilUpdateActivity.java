package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.tutkal.spider.R;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.ActivityProfilUpdateBinding;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.User;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfilUpdateActivity extends AppCompatActivity {

    private ActivityProfilUpdateBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profil_update);
        binding= DataBindingUtil.setContentView(ProfilUpdateActivity.this,R.layout.activity_profil_update);
        init();
        final User user=new User();
        user.setUserID(getSharedUserID());
        Ezrak.getInstance().getUser(user).UserProfil(binding);
        binding.ActivityProfilUpdateButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.ActivityProfilUpdateButtonRegister.setClickable(false);
                Ezrak.getInstance().getUser(user).ProfilUpdate(getApplicationContext(),new IEzrak() {
                    @Override
                    public void callback(Object data) {
                        switch ((String)data){
                            case "1":
                                Toast.makeText(ProfilUpdateActivity.this, "Bilgileriniz Güncellendi.", Toast.LENGTH_SHORT).show();
                                binding.ActivityProfilUpdateButtonRegister.setClickable(true);
                                startActivity(new Intent(ProfilUpdateActivity.this,ProfilActivity.class));
                                finish();
                                break;
                            case "-1":
                                Toast.makeText(ProfilUpdateActivity.this, "Bilgileriniz Güncellenemedi.", Toast.LENGTH_SHORT).show();
                                binding.ActivityProfilUpdateButtonRegister.setClickable(true);
                                break;
                            case "0":
                                Toast.makeText(ProfilUpdateActivity.this, "Şifreler Aynı Değil", Toast.LENGTH_SHORT).show();
                                binding.ActivityProfilUpdateButtonRegister.setClickable(true);
                                break;
                                default:
                                    binding.ActivityProfilUpdateButtonRegister.setClickable(true);
                                    Toast.makeText(ProfilUpdateActivity.this, "Bilinmeyen Bir Hata Oluştu", Toast.LENGTH_SHORT).show();
                                    break;
                        }
                    }
                });
            }
        });
        binding.ActivityProfilUpdateImageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                ProfilUpdateActivity.this.finish();
        }
        });
        binding.setProfilUpdate(user);
    }
    private void init() {
       if(getSupportActionBar() != null)
             getSupportActionBar().hide();
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(ProfilUpdateActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
}
