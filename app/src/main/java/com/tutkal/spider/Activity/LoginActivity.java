package com.tutkal.spider.Activity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.tutkal.spider.R;

import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.ActivityLoginBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.User;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity  {

    private ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();
        SharedControl();
        final User user =new User();
        user.setUserName(binding.ActivityLoginEditTextUsername.getText().toString());
        user.setPassword(binding.ActivityLoginEditTextPassword.getText().toString());
        binding.ActivityLoginButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.ActivityLoginProgressBar.setVisibility(View.VISIBLE);
                binding.ActivityLoginButtonLogin.setVisibility(View.GONE);

                Ezrak.getInstance().getUser(user).LoginControl(new IEzrak() {
                    @Override
                    public void callback(Object data) {
                        JSONObject objs = null;
                        String response = null;
                        int userID = 0;
                        try {
                            objs = new JSONObject((String) data);
                            response=objs.getString("response");
                            switch (response){
                                case "-1":
                                    Toast.makeText(LoginActivity.this, "Kullanıcı adı veya Şifre Yalnış", Toast.LENGTH_SHORT).show();
                                    break;
                                case "1":
                                    userID=objs.getInt("userID");
                                    Red.Instance.getShared().SharedSet(LoginActivity.this,"userID",String.valueOf(userID));
                                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(LoginActivity.this, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(LoginActivity.this, "=="+data, Toast.LENGTH_SHORT).show();
                        binding.ActivityLoginProgressBar.setVisibility(View.GONE);
                        binding.ActivityLoginButtonLogin.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
        binding.setUserrr(user);
    }



    private void init() {
        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        new Alagan(getApplicationContext());
        new Ezrak(getApplicationContext());
        new Red(getApplicationContext());
    }
    private void SharedControl() {
        Red.Instance.getShared().SharedGet(LoginActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                if(!id.equals("-1")){
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    finish();
                }
            }
        });
    }



}
