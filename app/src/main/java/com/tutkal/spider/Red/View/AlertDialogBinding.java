package com.tutkal.spider.Red.View;

import android.app.AlertDialog;
import android.content.Context;

import com.tutkal.spider.databinding.PopupAddTodoListBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateCashBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateContactBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateDeviceBinding;
import com.tutkal.spider.databinding.PopupInformationTodoListBinding;

public class AlertDialogBinding {


    public AlertDialogBinding(Context mContext) {
    }

    public AlertDialog PopupToDoListInfarmation(Context context, PopupInformationTodoListBinding binding){
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setView(binding.getRoot());
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }
    public AlertDialog PopupToDoListAdd(Context context, PopupAddTodoListBinding binding){
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setView(binding.getRoot());
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }
    public AlertDialog PopupCusstomerInformationContactUpdate(Context context, PopupCustomerUpdateContactBinding binding){
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setView(binding.getRoot());
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }
    public AlertDialog PopupCusstomerInformationDeviceUpdate(Context context, PopupCustomerUpdateDeviceBinding binding){
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setView(binding.getRoot());
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }
    public AlertDialog PopupCusstomerInformationFinancialUpdate(Context context, PopupCustomerUpdateCashBinding binding){
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setView(binding.getRoot());
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }

}
