package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import br.com.goncalves.pugnotification.notification.PugNotification;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.smarteist.autoimageslider.SliderView;
import com.tutkal.spider.Activity.Customer.CustomerAddActivity;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.SliderAdapter;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.Red.View.SliderViews;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ImageView imageCustomer,imageStatistics ,imageStocks;
    private TextView txtName,txtRevenue,txtExpense;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        new Red(getApplicationContext());
        /*Alagan.Instance.dbString
                .put("command","homepageUserDetail")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("asdasd",response);
                    }
                });*/
        //IncomeExpenseStatistics();
        //LastRegulations();
        //MaintenanceDate();
        //LastAddedCustomer();
        //ToDoList();
        getListt();

    }

    private void getListt() {
        Alagan.Instance.dbString
                .put("command","homePageMobileNew")
                .put("userID", String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e( "onResponse: ", response);
                        try {
                            JSONObject object=new JSONObject(response);
                            txtName.setText(object.getString("namesurname"));
                            txtRevenue.setText(object.getString("monthrevenue")+" TL");
                            JSONArray arrayIstatistik=object.getJSONArray("son_yedi_gun_gelir");

                            String[] axisData = new String[7];
                            int[] yAxisData = new int[7];
                            for (int i = 0; i <arrayIstatistik.length() ; i++) {
                                JSONObject obj = arrayIstatistik.getJSONObject(i);
                                axisData[i]=obj.getString("day");
                                yAxisData[i]=obj.getInt("total");
                            }
                            LineChartView lineChartView;
                            lineChartView = findViewById(R.id.chart);
                            List yAxisValues = new ArrayList();
                            List axisValues = new ArrayList();
                            Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));
                            for (int i = 0; i < axisData.length; i++) {
                                axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
                            }
                            for (int i = 0; i < yAxisData.length; i++) {
                                yAxisValues.add(new PointValue(i, yAxisData[i]));
                            }
                            List lines = new ArrayList();
                            lines.add(line);
                            LineChartData data = new LineChartData();
                            data.setLines(lines);
                            line.setColor(Color.parseColor("#f8833e"));
                            Axis axis = new Axis();
                            axis.setValues(axisValues);
                            axis.setTextSize(12);
                            axis.setTextColor(Color.parseColor("#999797"));
                            data.setAxisXBottom(axis);
                            Axis yAxis = new Axis();
                            yAxis.setTextColor(Color.parseColor("#999797"));
                            yAxis.setTextSize(12);
                            data.setAxisYLeft(yAxis);
                            lineChartView.setLineChartData(data);
                            Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
                            viewport.top = 1000;
                            lineChartView.setLineChartData(data);
                            Viewport viewport2 = new Viewport(lineChartView.getMaximumViewport());

                            lineChartView.setMaximumViewport(viewport);
                            lineChartView.setCurrentViewport(viewport2);

                            SliderView sliderLastRegulations = findViewById(R.id.Image_Slider_Latest_Regulations);
                            SliderViews sliderViews=new SliderViews();

                            ArrayList<RecyclerClass> arrayListcustomerAdded=new ArrayList<>();
                            JSONArray arraySonDuzenleme = new JSONArray(object.getString("son_duzenlemeler"));
                            for (int i = 0; i<arraySonDuzenleme.length();i++)
                            {
                                JSONObject SonDuzenleme = arraySonDuzenleme.getJSONObject(i);
                                arrayListcustomerAdded.add(new RecyclerClass(SonDuzenleme.getInt("id"),SonDuzenleme.getString("namesurname"),SonDuzenleme.getString("type")));
                            }
                            SliderAdapter sliderAdapter=sliderViews.SliderAdapter(getApplicationContext(),sliderLastRegulations,arrayListcustomerAdded,3);
                            sliderAdapter.notifyDataSetChanged();


                             SliderView sliderLastAddCustomer = findViewById(R.id.Image_Slider_Last_Add_Customer);
                             SliderViews sliderViews2=new SliderViews();
                             ArrayList<RecyclerClass> arrayList2=new ArrayList<>();

                            JSONArray jsonArray2 = new JSONArray(object.getString("son_bes_musteri"));
                            for (int i = 0; i<jsonArray2.length();i++)
                            {
                                JSONObject objs = jsonArray2.getJSONObject(i);
                                arrayList2.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("created_date")));
                            }
                            SliderAdapter sliderAdapter2= sliderViews2.SliderAdapter(getApplicationContext(),sliderLastAddCustomer,arrayList2,1);
                            sliderAdapter2.notifyDataSetChanged();

                             SliderView sliderMaintenanceDate = findViewById(R.id.Image_Slider_Maintenance_Date);

                             SliderViews sliderViews3=new SliderViews();
                             ArrayList<RecyclerClass> arrayList4=new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(object.getString("bakim_tarihi_yaklasanlar"));
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                // Log.e("bakım günü", objs.toString() );
                                arrayList4.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("day")+" Gün"));

                            }
                            SliderAdapter sliderAdapter3=sliderViews3.SliderAdapter(getApplicationContext(),sliderMaintenanceDate,arrayList4,2);
                            sliderAdapter3.notifyDataSetChanged();
                            TextView[] todo=new TextView[3];
                            JSONArray jsonArray3 = new JSONArray(object.getString("son_uc_is"));
                            todo[0]=findViewById(R.id.textView78);
                            todo[1]=findViewById(R.id.textView124);
                            todo[2]=findViewById(R.id.textView125);
                            for (int i = 0; i<jsonArray3.length();i++)
                            {
                                // Log.e( "sayaç",""+i);
                                JSONObject objs = jsonArray3.getJSONObject(i);
                                todo[i].setText(objs.getString("tittle"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    private void init() {

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        imageCustomer=findViewById(R.id.Activity_Main_BottomNavigation_Customers);
        imageStatistics=findViewById(R.id.Activity_Main_BottomNavigation_Statistics);
        imageStocks=findViewById(R.id.Activity_Main_BottomNavigation_Stock);
        txtName=findViewById(R.id.Activity_Main_Text_Name);
        txtRevenue=findViewById(R.id.Activity_Main_Text_Revenue);
        txtExpense=findViewById(R.id.Activity_Main_Text_Expense);
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.CALL_PHONE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();

    }

    private void IncomeExpenseStatistics() {


        //https://www.codingdemos.com/draw-android-line-chart/
        Alagan.Instance.dbString
                .put("command","homePageMobile")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject objs = new JSONObject(response);
                            txtName.setText(objs.getString("namesurname"));
                            txtRevenue.setText(objs.getString("revenue"));
                            txtExpense.setText(objs.getString("expense"));
                            JSONArray jsonArray = new JSONArray(objs.getString("statics"));
                            String[] axisData = new String[7];
                            int[] yAxisData = new int[7];
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject obje = jsonArray.getJSONObject(i);
                                axisData[i]=obje.getString("day");
                                yAxisData[i]=obje.getInt("value");
                                Log.d( "onResponse: day",obje.getString("day"));
                                Log.d( "onResponse: value",obje.getString("value"));
                            }
                            LineChartView lineChartView;

                            lineChartView = findViewById(R.id.chart);
                            List yAxisValues = new ArrayList();
                            List axisValues = new ArrayList();


                            Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));

                            for (int i = 0; i < axisData.length; i++) {
                                axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
                            }

                            for (int i = 0; i < yAxisData.length; i++) {
                                yAxisValues.add(new PointValue(i, yAxisData[i]));
                            }

                            List lines = new ArrayList();
                            lines.add(line);

                            LineChartData data = new LineChartData();
                            data.setLines(lines);
                            line.setColor(Color.parseColor("#f8833e"));
                            Axis axis = new Axis();
                            axis.setValues(axisValues);
                            axis.setTextSize(12);
                            axis.setTextColor(Color.parseColor("#999797"));
                            data.setAxisXBottom(axis);

                            Axis yAxis = new Axis();

                            yAxis.setTextColor(Color.parseColor("#999797"));
                            yAxis.setTextSize(12);
                            data.setAxisYLeft(yAxis);

                            lineChartView.setLineChartData(data);
                            Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
                            viewport.top = 1000;

                            lineChartView.setLineChartData(data);
                            Viewport viewport2 = new Viewport(lineChartView.getMaximumViewport());

                            lineChartView.setMaximumViewport(viewport);
                            lineChartView.setCurrentViewport(viewport2);

                        }catch (Exception e){

                        }
                    LastRegulations();
                    }
                });


    }
    private void LastRegulations() {

        final SliderView sliderLastRegulations = findViewById(R.id.Image_Slider_Latest_Regulations);
        final SliderViews sliderViews=new SliderViews();
        final ArrayList<RecyclerClass> arrayListcustomerAdded=new ArrayList<>();
        Alagan.Instance.dbString
                .put("command","alteration")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("count","5")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("sonuc",response);
                        try {
                            Log.e("sonuc",response);
                            //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = new JSONArray(response);
                           //Log.e("son Değişiklikler",response);
                            final ArrayList<RecyclerClass> arrayListcustomerAdded=new ArrayList<>();
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                arrayListcustomerAdded.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("type")));
                            }
                            SliderAdapter sliderAdapter=sliderViews.SliderAdapter(getApplicationContext(),sliderLastRegulations,arrayListcustomerAdded,3);
                            sliderAdapter.notifyDataSetChanged();

                        }catch (Exception e){

                        }
                       MaintenanceDate();
                    }
                });

    }
    private void MaintenanceDate() {
        Log.e("sonuc","MaintenanceDate");
        final SliderView sliderMaintenanceDate = findViewById(R.id.Image_Slider_Maintenance_Date);
        final SliderViews sliderViews=new SliderViews();
        final ArrayList<RecyclerClass> arrayList=new ArrayList<>();
       // Log.e("bakım günü","girdi" );
        Alagan.Instance.dbString
                .put("command","maintenanceDateArrivals")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();

                            final SliderView sliderMaintenanceDate = findViewById(R.id.Image_Slider_Maintenance_Date);
                            final SliderViews sliderViews=new SliderViews();
                            final ArrayList<RecyclerClass> arrayList=new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                               // Log.e("bakım günü", objs.toString() );
                                arrayList.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("day")+"Gün"));
                            }
                            SliderAdapter sliderAdapter=sliderViews.SliderAdapter(getApplicationContext(),sliderMaintenanceDate,arrayList,2);
                            sliderAdapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }
                        LastAddedCustomer();
                    }
                });

    }
    private void LastAddedCustomer() {
        Log.e("sonuc","LastAddedCustomer");
        final SliderView sliderLastAddCustomer = findViewById(R.id.Image_Slider_Last_Add_Customer);
        final SliderViews sliderViews=new SliderViews();
        final ArrayList<RecyclerClass> arrayList=new ArrayList<>();
        /*for (int i = 0; i <5 ; i++) {
            arrayList.add(new RecyclerClass(4,"Kadir Özen","Değişken"));
        }*/
        Alagan.Instance.dbString
                .put("command","customerLastAddedList")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("count","5")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(), ""+(response), Toast.LENGTH_SHORT).show();
                        try {
                            final SliderView sliderLastAddCustomer = findViewById(R.id.Image_Slider_Last_Add_Customer);
                            final SliderViews sliderViews=new SliderViews();
                            final ArrayList<RecyclerClass> arrayList=new ArrayList<>();

                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                arrayList.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("created_date")));
                            }
                            SliderAdapter sliderAdapter= sliderViews.SliderAdapter(getApplicationContext(),sliderLastAddCustomer,arrayList,1);
                            sliderAdapter.notifyDataSetChanged();

                        }catch (Exception e){
                        }
                       ToDoList();
                    }
                });
    }
    private void ToDoList(){
        Log.e("sonuc","ToDoList");
        final TextView[] todo=new TextView[3];
        todo[0]=findViewById(R.id.textView78);
        todo[1]=findViewById(R.id.textView124);
        todo[2]=findViewById(R.id.textView125);
        Alagan.Instance.dbString
                .put("command","toDoLastAdded")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            //Log.e( "sayaç",""+response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                               // Log.e( "sayaç",""+i);
                                JSONObject objs = jsonArray.getJSONObject(i);
                                todo[i].setText(objs.getString("tittle"));

                            }
                        }catch (Exception e){

                        }

                    }
                });

    }
    public void ActivityMainImageOnClick(View view) {
        switch (view.getId()){
            case R.id.ActivityMain_Image_AddCustomer:
                ImageView imageView=findViewById(R.id.ActivityMain_Image_AddCustomer);
                imageView.setClickable(false);
                startActivity(new Intent(getApplicationContext(), CustomerAddActivity.class));
                imageView.setClickable(true);
                break;
            case R.id.Activity_Main_Image_Profile:
                ImageView imageView3=findViewById(R.id.Activity_Main_Image_Profile);
                imageView3.setClickable(false);
                startActivity(new Intent(getApplicationContext(),ProfilActivity.class));
                imageView3.setClickable(true);
                break;
            case R.id.Activity_Main_Image_Setting:
                ImageView imageView2=findViewById(R.id.Activity_Main_Image_Setting);
                imageView2.setClickable(false);
                startActivity(new Intent(getApplicationContext(),SettingActivity.class));
                imageView2.setClickable(true);
                break;
        }
    }
    public void ActivityMainCard(View view) {
        switch (view.getId()){
            case R.id.ActivityMain_ConstraintLayout_ToDoList:
                startActivity(new Intent(getApplicationContext(),ToDoListActivity.class));
                break;
            case R.id.ActivityMain_ConstraintLayout_Statics:
                startActivity(new Intent(getApplicationContext(),IncomeExpenseStatisticsActivity.class));
                break;
            case R.id.chart:
                startActivity(new Intent(getApplicationContext(),IncomeExpenseStatisticsActivity.class));
                break;
        }
    }
    public void ActivityMainBottomNavigationOnClick(View view) {
        switch (view.getId()){
            case R.id.Activity_Main_BottomNavigation_Customers:
                ImageView imageView=findViewById(R.id.Activity_Main_BottomNavigation_Customers);
                imageView.setClickable(false);
                startActivity(new Intent(getApplicationContext(), CustomerListActivity.class));
                imageView.setClickable(true);
                break;
            case R.id.Activity_Main_BottomNavigation_Statistics:
                imageCustomer.setColorFilter(Color.parseColor("#c8c8c8"));
                imageStatistics.setColorFilter(Color.parseColor("#f8833e"));
                ImageView imageView3=findViewById(R.id.Activity_Main_BottomNavigation_Customers);
                imageView3.setClickable(false);
                startActivity(new Intent(getApplicationContext(),IncomeExpenseStatisticsActivity.class));
                imageView3.setClickable(true);
                break;
            case R.id.Activity_Main_BottomNavigation_Stock:
                imageStocks.setColorFilter(Color.parseColor("#f8833e"));
                imageCustomer.setColorFilter(Color.parseColor("#c8c8c8"));
                ImageView imageView4=findViewById(R.id.Activity_Main_BottomNavigation_Customers);
                imageView4.setClickable(false);
                startActivity(new Intent(getApplicationContext(),DebtListActivity.class));
                imageView4.setClickable(true);
                break;
        }
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(MainActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
    @Override
    protected void onResume() {
        super.onResume();
        imageCustomer.setColorFilter(Color.parseColor("#f8833e"));
        imageStatistics.setColorFilter(Color.parseColor("#c8c8c8"));
        imageStocks.setColorFilter(Color.parseColor("#c8c8c8"));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        getListt();
        super.onRestart();
    }
}

