package com.tutkal.spider.Red.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class PageAdapter extends FragmentPagerAdapter {



    private  int numOfTabs;
    Fragment arg[]=new Fragment[10];
    public PageAdapter(@NonNull FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }
    public void asda(Fragment... args){
        for(int i=0; i<numOfTabs; i++){
            this.arg[i]=args[i];
        }

    }

    @Override
    public Fragment getItem(int position) {
        return arg[position];
    }


    @Override
    public int getCount() {
        return numOfTabs;
    }
}
