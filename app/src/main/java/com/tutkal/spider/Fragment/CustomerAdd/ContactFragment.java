package com.tutkal.spider.Fragment.CustomerAdd;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tutkal.spider.databinding.FragmentAddContactBinding;
import com.tutkal.spider.ezrak.database.Customer;


public class ContactFragment extends Fragment {

    public ContactFragment() {
        instance = this;
    }
    private Customer customer=new Customer();
    private static ContactFragment instance;
    public static ContactFragment getInstance()
    {
        return instance;
    }
    private FragmentAddContactBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // View view =inflater.inflate(R.layout.fragment_add_contact, container, false);
        binding=FragmentAddContactBinding.inflate(inflater,container,false);

        binding.setCustomerAdd(customer);
        return binding.getRoot();
    }

    public Customer GetCustomerAddInformation()
    {
        return customer ;
    }
}
