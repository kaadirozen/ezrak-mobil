package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tutkal.spider.Activity.Customer.CustomerInformationActivitiy;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LastAddedCustomerActivity extends AppCompatActivity {

    private ArrayList<RecyclerClass> arrayList=new ArrayList<>();
    private  RecyclerView recyclerView;
    final RecyclerViews k = Red.Instance.getView().GetRecyclerViews();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_added_customer);
        init();







        k.addItem(3,R.id.Item_ToDoList_Name,R.id.Item_ToDoList_Date,
                R.id.Item_ToDoList_Counter)
                .RecyclerView(recyclerView, arrayList, R.layout.item_latestregulations_and_todolist, new RecyclerViewOnClick() {
                    @Override
                    public void OnClick(int ID) {
                        Red.Instance.getShared().SharedSet(getApplicationContext(),"customerID",String.valueOf(ID));
                        startActivity(new Intent(LastAddedCustomerActivity.this, CustomerInformationActivitiy.class));
                    }
                }).Creat();



    }

    private void init() {
        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        recyclerView=findViewById(R.id.Activity_LastAddedCustomer_Recycler);

    }

    public void ActivityLastAddedCustomerImageClick(View view) {
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(LastAddedCustomerActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }

    @Override
    protected void onResume() {
        arrayList.clear();
        super.onResume();Alagan.Instance.dbString
                .put("command","customerLastAddedList")
                .put("userID",String.valueOf(getSharedUserID()))
                .put("count","250")
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                arrayList.add(new RecyclerClass(objs.getInt("id"),objs.getString("namesurname"),objs.getString("created_date")));
                            }
                            k.recyclerViewAdapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }
                    }
                });

    }
}
