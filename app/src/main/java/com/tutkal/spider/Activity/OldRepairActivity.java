package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.furkanbahadirozdemir.fountain.Fountain;
import com.furkanbahadirozdemir.fountain.Interface.IListViewClick;
import com.furkanbahadirozdemir.fountain.Interface.IRecyclerView;
import com.furkanbahadirozdemir.fountain.List.RecyclerView.FountainRecyclerView;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OldRepairActivity extends AppCompatActivity {



    private ArrayList<RecyclerClass> arrayList=new ArrayList<>();
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_repair);



        init();

        final FountainRecyclerView view = Fountain.Initialize(getApplicationContext()).List().RecyclerView();
        //alagann
        Alagan.Instance.dbString
                .put("command","customerOldMaintenanceList")
                .put("id",String.valueOf(getSharedCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                Log.e("device detail",objs.toString());
                                arrayList.add(new RecyclerClass(4,objs.getString("device_model"),objs.getString("note"),objs.getString("date"),objs.getString( "device_detail")));
                                Log.e("obj",objs.toString());
                            }
                            view.notifyDataSetChanged();
                        }catch (Exception e){


                        }
                    }
                });


        view.SetContext(getApplicationContext());
        view.SetItemList(arrayList);
        view.SetLayout(R.layout.item_previous_maintenance);
        final int[] counter = {1};
        view.SetListener(new IRecyclerView() {
            @Override
            public void OnView(View view, Object obj) {
                final RecyclerClass mClass=(RecyclerClass)obj;

                TextView txtCounter=view.findViewById(R.id.Item_PreviousMaintenance_Counter);
                TextView txtCihazModeli=view.findViewById(R.id.Item_PreviousMaintenance_DeviceModel);
                TextView txtYapilanİslem=view.findViewById(R.id.Item_PreviousMaintenance_Transaction);
                TextView txtYapilanBakimTarihi=view.findViewById(R.id.Item_PreviousMaintenance_MaintenanceDate);

                CheckBox chk5Mikron=view.findViewById(R.id.checkBox6);
                CheckBox chkBlokKarbon=view.findViewById(R.id.checkBox7);
                CheckBox chkSonKarbon=view.findViewById(R.id.checkBox8);
                CheckBox chkKarbonFiltre=view.findViewById(R.id.checkBox9);
                CheckBox chkMebrahFiltre=view.findViewById(R.id.checkBox10);

                txtCihazModeli.setText(mClass.getAs()[0]);
                txtYapilanİslem.setText(mClass.getAs()[1]);
                txtYapilanBakimTarihi.setText(mClass.getAs()[2]);

                String[] parts = mClass.getAs()[3].split("-");

                chk5Mikron.setChecked(isChecked(parts[0]));
                chkBlokKarbon.setChecked(isChecked(parts[1]));
                chkSonKarbon.setChecked(isChecked(parts[2]));
                chkKarbonFiltre.setChecked(isChecked(parts[3]));
                chkMebrahFiltre.setChecked(isChecked(parts[4]));

                txtCounter.setText(""+ counter[0]);



                counter[0] = counter[0] +1;
            }

            @Override
            public void OnViewHolder(View view) {

            }
        });
        view.Create(recyclerView);
        RecyclerView.LayoutManager mLayoutmanager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutmanager);
        view.SetClickListener(new IListViewClick() {
            @Override
            public void OnClick(int position, Object data) {

            }
        });




    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        recyclerView=findViewById(R.id.Activity_OldRepair_Recycler);
        ImageView imgBack=findViewById(R.id.imageView18);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(OldRepairActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(getApplicationContext(), "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }
    private Boolean isChecked(String data){
        if(data.equals("1")){
            return true;
        }else if(data.equals("0")){
            return false;
        }
        return null;
    }
}
