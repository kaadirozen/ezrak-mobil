package com.tutkal.spider.ezrak.database.Statistics;

import android.content.Context;

public class Statistics {

    private Context mContext;
    private IncomeExpense incomeExpense;
    private Maintenance maintenance;


    public Statistics(Context mContext) {
        this.incomeExpense=getIncomeExpense();
        this.maintenance=getMaintenance();
    }


    public  IncomeExpense getIncomeExpense() {
        return new IncomeExpense(mContext);
    }

    public  Maintenance getMaintenance() {
        return new  Maintenance();
    }



}
