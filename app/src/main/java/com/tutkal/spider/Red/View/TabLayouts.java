package com.tutkal.spider.Red.View;

import android.content.Context;
import com.google.android.material.tabs.TabLayout;
import com.tutkal.spider.Red.Adapter.PageAdapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;


public class TabLayouts {

    Context mcontext;
    private PageAdapter pageAdapter;


    public TabLayouts(Context context) {
        this.mcontext = context;
    }

    public TabLayouts PageAdapter (TabLayout tabLayoutt, ViewPager viewPager, FragmentManager fragmentManager, int getItemTabCount, Fragment... args){
        pageAdapter=new PageAdapter(fragmentManager,getItemTabCount);
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutt));
        return this;
    }

    public void AddFragment(Fragment... args) {
        pageAdapter.asda(args);

    }


}
