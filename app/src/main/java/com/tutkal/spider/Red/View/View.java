package com.tutkal.spider.Red.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.renderscript.ScriptGroup;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;


import com.tutkal.spider.DataBinderMapperImpl;
import com.tutkal.spider.R;
import com.tutkal.spider.databinding.PopupInformationTodoListBinding;
import com.tutkal.spider.ezrak.IEzrak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.databinding.Bindable;
import androidx.databinding.BindingMethod;
import androidx.databinding.DataBindingUtil;

public class View {

    private Context mContext;
    private Spinner spinner;
    public TabLayouts TabLayout;
    public RecyclerViews RecyclerView;
    private Calendar myCalendar =null;
    private DatePickerDialog.OnDateSetListener date;


    public View(Context context) {
        this.mContext = context;
        TabLayout=new TabLayouts(context);

    }
    public RecyclerViews
    GetRecyclerViews()
    {
        return new RecyclerViews(mContext);
    }
    public View Spinner(Spinner spinner, String... args){
        this.spinner=spinner;
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item,args);
        //adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinner.setAdapter(adapter);
        return this;
    }
    public AlertDialog AlertDiyalog(Context context,int layoutID){
        // Create a AlertDialog Builder.
        /*
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ToDoListActivity.this);

        alertDialogBuilder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(ToDoListActivity.this);
        popupInputDialogView = layoutInflater.inflate(R.layout.popup_add_todo_list, null);
        alertDialogBuilder.setView(popupInputDialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        TextView txtExit=alertDialog.findViewById(R.id.textView131);
        txtExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });*/
        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        android.view.View popupInputDialogView = layoutInflater.inflate(layoutID,null);
        alertDialogBuilder.setView(popupInputDialogView);
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
        return alertDialog;
    }
    public void AlertDiyalogTrueFalse(Context context, String title, String message, final IEzrak IEzrak){
        final boolean[] deger = new boolean[1];
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                IEzrak.callback(false);
            }
        });
        builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                IEzrak.callback(true);

            }
        });
        builder.show();

    }
    public AlertDialogBinding getAlertDialogBinding() {
        return new AlertDialogBinding(mContext);
    }
    public void getDate(Context mContext, final Calendar myCalendar, final IEzrak IEzrak){
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                IEzrak.callback(sdf);

            }

        };

    }
    public Calendar getCalander(Context mContext,Calendar calendar){
        this.myCalendar=calendar;

        new DatePickerDialog(mContext,date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        return myCalendar;

    }
}


