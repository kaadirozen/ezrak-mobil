package com.tutkal.spider.Activity.Customer;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.tutkal.spider.Activity.PaymentHistoryActivity;
import com.tutkal.spider.Activity.ProfilActivity;
import com.tutkal.spider.Activity.ToDoListActivity;
import com.tutkal.spider.Fragment.CustomerAdd.DeviceInformationFragment;
import com.tutkal.spider.Fragment.CustomerInformation.ContactFragment;
import com.tutkal.spider.Fragment.CustomerInformation.DeviceInfarmationFragment;
import com.tutkal.spider.Fragment.CustomerInformation.FinancialFragment;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.FragmentAddDeviceInformationBinding;
import com.tutkal.spider.databinding.FragmentInformationDeviceInfarmationBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateCashBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateContactBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateDeviceBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.Customer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CustomerInformationActivitiy extends AppCompatActivity {

    private View popupInputDialogView = null;
    private TabLayout tabLayout;
    private ImageView imageViewSms,imageViewEdit;
    private String smsKalanGun,smsTaksit;
    private final Calendar myCalendar = Calendar.getInstance();
    private final Calendar myCalendar1 = Calendar.getInstance();
    private final Calendar myCalendar2 = Calendar.getInstance();
    private ViewPager viewPager;
    private DatePickerDialog.OnDateSetListener date,date1,date2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_information);
        init();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                imageViewEdit.setVisibility(View.VISIBLE);

                if(tab.getPosition()==1){
                    imageViewSms.setVisibility(View.VISIBLE);


                } if(tab.getPosition()==2){
                    imageViewSms.setVisibility(View.VISIBLE);

                } if(tab.getPosition()==0){

                    imageViewSms.setVisibility(View.GONE);
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    private void init() {
        imageViewSms=findViewById(R.id.Activity_CustomerInformation_ImageView_Sms);
        imageViewEdit=findViewById(R.id.Activity_CustomerInformation_ImageView_Edit);
        imageViewSms.setVisibility(View.GONE);
        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        tabLayout=findViewById(R.id.CustomerInfarmationTabLayout);
        viewPager=findViewById(R.id.CustomerInfarmationViewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        Red.Instance.getView().TabLayout.PageAdapter(tabLayout,viewPager,getSupportFragmentManager(),tabLayout.getTabCount())
                .AddFragment(new ContactFragment(),new DeviceInfarmationFragment(),new FinancialFragment());
    }
    public void ActivityCustomerInformationImageVieClick(View view) {
        switch (view.getId()){
            case R.id.Activity_CustomerInformation_ImageView_Back:
                onBackPressed();
                finish();
                break;
            case R.id.Activity_CustomerInformation_ImageView_Sms:
                Alagan.Instance.dbString
                        .put("command","userDetail")
                        .put("userID",String.valueOf(getSharedUserID()))
                        .read(new AlaganStringDatabase.AlaganListener() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject objs = new JSONObject(response);
                                    Intent mesajGonder = new Intent(Intent.ACTION_VIEW);
                                    mesajGonder.setData(Uri.parse("sms:" +ContactFragment.getIntance().Call()));

                                    if(tabLayout.getSelectedTabPosition()==1){
                                        smsKalanGun = "Değerli müşterimiz su arıtma cihazınız bakım zamanı gelmiştir.Uygun zamanda bizimle iletişime geçiniz.\n"
                                                +objs.getString("storename")+" "+objs.getString("namesurname")+"\n"
                                                +objs.getString("phone1")+"\n"
                                                +objs.getString("phone2")+"\n"
                                                +objs.getString("phone3")+"\n";

                                        mesajGonder.putExtra("sms_body", smsKalanGun);
                                        startActivity(mesajGonder);
                                    }else if(tabLayout.getSelectedTabPosition()==2){

                                        smsTaksit="Değerli müşterimiz su arıtma cihazınız "+ FinancialFragment.getInstance().getCustomer().getKalanUcret()+" TL borcunuz bulunmaktadır. \n"
                                                +objs.getString("storename")+" "+objs.getString("namesurname")+"\n"
                                                +objs.getString("phone1")+"\n"
                                                +objs.getString("phone2")+"\n"
                                                +objs.getString("phone3")+"\n";
                                        mesajGonder.putExtra("sms_body", smsTaksit);
                                        startActivity(mesajGonder);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                break;
            case R.id.Activity_CustomerInformation_ImageView_Call:
                String phone=ContactFragment.getIntance().Call();
                try {
                    Intent cal=new Intent(Intent.ACTION_DIAL);
                    cal.setData(Uri.parse("tel: "+phone));
                    startActivity(cal);
                }catch (Error e){
                }
                break;
            case R.id.Activity_CustomerInformation_ImageView_Edit:
                switch (tabLayout.getSelectedTabPosition()){
                    case 0:
                        ContactUpdate();
                        break;
                    case 1:
                        DeviceUpdate();
                        break;
                    case 2:
                        if(FinancialFragment.getInstance().getPaymentMethod()){
                            FinanciaUpdate();
                        }else{
                            startActivity(new Intent(getApplicationContext(), PaymentHistoryActivity.class));
                        }

                        break;
                }
                break;
        }

    }
    private void ContactUpdate(){
        PopupCustomerUpdateContactBinding binding= DataBindingUtil.inflate(LayoutInflater.from(CustomerInformationActivitiy.this),
                R.layout.popup_customer_update_contact,null,false);
        final android.app.AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding()
                .PopupCusstomerInformationContactUpdate(CustomerInformationActivitiy.this,binding);
        final Customer customer=ContactFragment.getIntance().getCustomer();
        customer.setCustomerID(getSharedCustomerID());
        binding.PopupCustomerUpdateContactEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Red.Instance.getView().AlertDiyalogTrueFalse(CustomerInformationActivitiy.this, "Müşteri Düzenleme",
                        "Müşteriyi Düzenlemek İstediğinize Emin misiniz?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Ezrak.getInstance().getCustomer().CustomerUpdateConctant(customer);
                                    finish();
                                    startActivity(getIntent());
                                }
                            }
                        });

            }
        });
        binding.PopupCustomerUpdateContactExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.setCustomerUpdate(customer);
    }
    private void DeviceUpdate(){
        final PopupCustomerUpdateDeviceBinding binding= DataBindingUtil.inflate(LayoutInflater.from(CustomerInformationActivitiy.this),
                R.layout.popup_customer_update_device,null,false);
        final android.app.AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding()
                .PopupCusstomerInformationDeviceUpdate(CustomerInformationActivitiy.this,binding);
        final Customer customer=DeviceInfarmationFragment.getInstance().getCustomer();
        FragmentInformationDeviceInfarmationBinding binding2=DeviceInfarmationFragment.getInstance().binding();
        binding.editText34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(CustomerInformationActivitiy.this,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        binding.editText35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(CustomerInformationActivitiy.this,date1, myCalendar1
                        .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                        myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tarihIslemleriDeviceUpdate(binding);
        binding.PopupCustomerUpdateDeviceCheckBoxBlokKarbo.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxBlokKarbon.isChecked());
        binding.PopupCustomerUpdateDeviceCheckBoxKarbonFiltre.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxKarbonFiltre.isChecked());
        binding.PopupCustomerUpdateDeviceCheckBoxMebrahFiltre.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxMebrahFiltre.isChecked());
        binding.PopupCustomerUpdateDeviceCheckBoxSonKarbon.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxSonKarbon.isChecked());
        binding.PopupCustomerUpdateDeviceCheckBoxMikron5.setChecked(binding2.FragmentCustomerInformationDeviceCheckBoxMikron5.isChecked());
        customer.setCustomerID(getSharedCustomerID());
        binding.PopupCustomerUpdateDeviceTextViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                //Ezrak.getInstance().getCustomer().CustomerUpdateDevice(customer);
                Red.Instance.getView().AlertDiyalogTrueFalse(CustomerInformationActivitiy.this, "Müşteri Düzenleme",
                        "Müşteriyi Düzenlemek İstediğinize Emin misiniz?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    customer.setBlokKarbon(binding.PopupCustomerUpdateDeviceCheckBoxBlokKarbo.isChecked());
                                    customer.setKarbonFiltre(binding.PopupCustomerUpdateDeviceCheckBoxKarbonFiltre.isChecked());
                                    customer.setMebrahFiltre(binding.PopupCustomerUpdateDeviceCheckBoxMebrahFiltre.isChecked());
                                    customer.setSonKarbon(binding.PopupCustomerUpdateDeviceCheckBoxSonKarbon.isChecked());
                                    customer.setMikron5(binding.PopupCustomerUpdateDeviceCheckBoxMikron5.isChecked());
                                    Ezrak.getInstance().getCustomer().CustomerUpdateDevice(customer,binding);
                                    finish();
                                    startActivity(getIntent());
                                }
                            }
                        });
            }
        });
        binding.PopupCustomerUpdateDeviceTextViewExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.setCustomerUpdate(customer);
    }
    private void FinanciaUpdate(){
        final PopupCustomerUpdateCashBinding binding= DataBindingUtil.inflate(LayoutInflater.from(CustomerInformationActivitiy.this),
                R.layout.popup_customer_update_cash,null,false);
        final android.app.AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding()
                .PopupCusstomerInformationFinancialUpdate(CustomerInformationActivitiy.this,binding);
        final Customer customer=FinancialFragment.getInstance().getCustomer();
        customer.setCustomerID(getSharedCustomerID());
        customer.setUserID(getSharedUserID());

        binding.PopupCustomerUpdateFinancialEdittTextOdenecekUcret.setText(customer.getKalanUcret());
        binding.PopupCustomerUpdateFinancialTextviewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Red.Instance.getView().AlertDiyalogTrueFalse(CustomerInformationActivitiy.this, "Ödeme",
                        "Ödeme Yapmak İstediğinize Emin misiniz?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Ezrak.getInstance().getCustomer().CustomerCashPayment(customer);
                                    finish();
                                    startActivity(getIntent());
                                }
                            }
                        });
            }
        });
        binding.PopupCustomerUpdateFinancialTextViewExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.setCustomerUpdate(customer);
    }
    private void tarihIslemleriDeviceUpdate(final PopupCustomerUpdateDeviceBinding binding) {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                binding.editText34.setText(sdf.format(myCalendar.getTime()));
            }
        };
        date1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar1.set(Calendar.YEAR, year);
                myCalendar1.set(Calendar.MONTH, monthOfYear);
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                binding.editText35.setText(sdf.format(myCalendar1.getTime()));
            }
        };
    }

    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(CustomerInformationActivitiy.this, "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }
    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(CustomerInformationActivitiy.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }

}
