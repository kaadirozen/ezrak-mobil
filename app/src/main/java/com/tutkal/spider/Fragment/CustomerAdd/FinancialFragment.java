package com.tutkal.spider.Fragment.CustomerAdd;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tutkal.spider.R;
import com.tutkal.spider.databinding.FragmentAddFinancialBinding;
import com.tutkal.spider.ezrak.database.Customer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class FinancialFragment extends Fragment {

    public FinancialFragment() {
        // Required empty public constructor
        instance=this;
    }

    private static FinancialFragment instance;
    public static FinancialFragment getInstance() {
        return instance;
    }
    private FragmentAddFinancialBinding binding;
    private Customer customer=new Customer();
    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // View view= inflater.inflate(R.layout.fragment_add_financial, container, false);
        binding=FragmentAddFinancialBinding.inflate(inflater,container,false);
        customer.setPaymentMethod("1");



        binding.FragmentCustomerAddFinancialRadioGroupPaymentMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                //nakit
                if(R.id.Fragment_CustomerAddFinancial_RadioButton_Cash ==radioGroup.getCheckedRadioButtonId()){
                    binding.FragmentCustomerAddFinancialConst.setVisibility(View.GONE);
                    binding.editText13.setText("0");
                    customer.setPaymentMethod("1");
                }//peşin
                else if(R.id.Fragment_CustomerAddFinancial_RadioButton_Cash_Installment ==radioGroup.getCheckedRadioButtonId()){
                    binding.FragmentCustomerAddFinancialConst.setVisibility(View.VISIBLE);

                    customer.setPaymentMethod("-1");
                }
            }
        });
        binding.editText14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tarihIslemleri();
        binding.setCustomerUpdate(customer);
        return binding.getRoot();
    }
    public Customer getCustomerAddFinancial(){


        return customer;
    }
    private void tarihIslemleri () {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                binding.editText14.setText(sdf.format(myCalendar.getTime()));
            }
        };
    }
}
