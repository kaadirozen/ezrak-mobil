package com.tutkal.spider.Activity;


import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.app.AlertDialog;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.databinding.PopupAddTodoListBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateDeviceBinding;
import com.tutkal.spider.databinding.PopupInformationTodoListBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.ToDoList;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ToDoListActivity extends AppCompatActivity {


    private ArrayList<?> arrayList;
    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        init();
        ToDoList toDoList=new ToDoList();
        toDoList.setUserID(getSharedUserID());
        final RecyclerViews k = Red.Instance.getView().GetRecyclerViews();
        arrayList=Ezrak.getInstance().getToDoList(toDoList).GetToDoList(k);
        RecyclerView recyclerView=findViewById(R.id.Activity_ToDoList_Recycler);
        k.addItem(3,R.id.Item_ToDoList_Name,R.id.Item_ToDoList_Date,
                R.id.Item_ToDoList_Counter)
                .RecyclerView(recyclerView, arrayList, R.layout.item_latestregulations_and_todolist, new RecyclerViewOnClick() {
                    @Override
                    public void OnClick(int ID) {
                        InformationAlertDialog(ID);
                    }
                }).Creat();
    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        ImageView imgBack=findViewById(R.id.Activity_ToDoList_ImageBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

    }

    private void AddAlertDialog() {
        PopupAddTodoListBinding binding=DataBindingUtil.inflate(LayoutInflater.from(ToDoListActivity.this),
                R.layout.popup_add_todo_list,null,false);
        final AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding().PopupToDoListAdd(ToDoListActivity.this,binding);
        final ToDoList toDoList=new ToDoList();
        toDoList.setUserID(getSharedUserID());
        binding.editText16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ToDoListActivity.this,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tarihIslemleri(binding);
        binding.PopupToDoListAddEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Ezrak.getInstance().getToDoList(toDoList).ToDoListAdd(new IEzrak() {
                    @Override
                    public void callback(Object data) {
                        Log.e( "callback: ",(String)data );
                        switch ((String)data){

                            case "1":
                                Toast.makeText(ToDoListActivity.this, "Yapılacak İş Eklendi", Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                                finish();
                                startActivity(getIntent());
                                break;
                            case "-1":
                                Toast.makeText(ToDoListActivity.this, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
            }
        });
        binding.PopupToDoListAddExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.setToDoListUpdate(toDoList);
    }

    private void InformationAlertDialog(final int TodoListID){
        PopupInformationTodoListBinding binding=DataBindingUtil.inflate(LayoutInflater.from(ToDoListActivity.this),
                R.layout.popup_information_todo_list,null,false);
        final AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding().PopupToDoListInfarmation(ToDoListActivity.this,binding);
        final ToDoList toDoList=new ToDoList();
        toDoList.setToDoListID(getSharedUserID());
        toDoList.setToDoListID(TodoListID);
        Ezrak.getInstance().getToDoList(toDoList).ToDoListInformation(binding);
        binding.PopupToDoListInformationEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                UpdateAlertDialog(TodoListID,toDoList.getToDoListName(),toDoList.getToDoListDate(),toDoList.getToDoListNote());
            }
        });
        binding.PopupToDoListInformationExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.PopupToDoListInformationDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToDoListDelete(TodoListID,alertDialog);
            }
        });
        binding.setToDoList(toDoList);
    }

    private void ToDoListDelete(int todoListID, final AlertDialog alertDialog) {
        Alagan.Instance.dbString
                .put("command","toDoDelete")
                .put("id",String.valueOf(todoListID))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e( "onResponse: ", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getString("response").equals("1")){
                                Toast.makeText(ToDoListActivity.this, "İş Silindi", Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                                finish();
                                startActivity(getIntent());
                            }else {
                                Toast.makeText(ToDoListActivity.this, "Hata oluştu", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void UpdateAlertDialog(int todoListID, String toDoListName, String toDoListDate, String toDoListNote){
        PopupAddTodoListBinding binding=DataBindingUtil.inflate(LayoutInflater.from(ToDoListActivity.this),
                R.layout.popup_add_todo_list,null,false);
        final AlertDialog alertDialog=Red.Instance.getView().getAlertDialogBinding().PopupToDoListAdd(ToDoListActivity.this,binding);
        final ToDoList toDoList=new ToDoList();
        toDoList.setUserID(getSharedUserID());
        toDoList.setToDoListID(todoListID);
        toDoList.setToDoListName(toDoListName);
        toDoList.setToDoListDate(toDoListDate);
        binding.editText16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ToDoListActivity.this,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tarihIslemleri(binding);
        toDoList.setToDoListNote(toDoListNote);
        binding.PopupToDoListAddEdit.setText("Düzenle");
        binding.PopupToDoListAddEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Ezrak.getInstance().getToDoList(toDoList).ToDoListUpdate(new IEzrak() {
                    @Override
                    public void callback(Object data) {
                        switch ((String)data){
                            case "1":
                                Toast.makeText(ToDoListActivity.this, "Yapılacak İş Güncellendi", Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                                finish();
                                startActivity(getIntent());
                                break;
                            case "-1":
                                Toast.makeText(ToDoListActivity.this, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                                break;
                        }

                    }
                });
            }
        });
        binding.PopupToDoListAddExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        binding.setToDoListUpdate(toDoList);

    }

    public void ActivityToDoListImageClick(View view) {
        switch (view.getId()){
            case R.id.Activity_ToDoList_ImageAdd:
                AddAlertDialog();
                break;
            case R.id.Activity_CustomerList_ImageFilter:
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
                break;

        }
    }
    private void tarihIslemleri(final PopupAddTodoListBinding bindingg) {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                bindingg.editText16.setText(sdf.format(myCalendar.getTime()));
            }
        };
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(ToDoListActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }

}
