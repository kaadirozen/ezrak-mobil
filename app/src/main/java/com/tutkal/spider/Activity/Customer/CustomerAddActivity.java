package com.tutkal.spider.Activity.Customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.tutkal.spider.Activity.MainActivity;
import com.tutkal.spider.Activity.ProfilActivity;
import com.tutkal.spider.Fragment.CustomerAdd.ContactFragment;
import com.tutkal.spider.Fragment.CustomerAdd.DeviceInformationFragment;
import com.tutkal.spider.Fragment.CustomerAdd.FinancialFragment;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.Customer;

public class CustomerAddActivity extends AppCompatActivity   {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add);
        init();
        ImageView imgBack=findViewById(R.id.imageView2);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });



        final ImageView imgAdd=findViewById(R.id.Activity_CustomerAdd_Button_Add);
        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customer customerContact,customerDeviceModel,customerFinancial;
                customerContact = ContactFragment.getInstance().GetCustomerAddInformation();
                customerDeviceModel=DeviceInformationFragment.getInstance().getCustomerAddDeviceInformation();
                customerFinancial=FinancialFragment.getInstance().getCustomerAddFinancial();
                if(customerContact.getNameSurname()==null || customerContact.getNameSurname().trim().isEmpty()){
                    Toast.makeText(CustomerAddActivity.this, "Ad ve Soyad Boş Bırakılamaz", Toast.LENGTH_SHORT).show();
                }else if((customerFinancial.getPaymentMethod() !=null &&  customerFinancial.getPaymentMethod().equals("-1"))&&
                        (customerFinancial.getInstallmentStartDate()==null || customerFinancial.getInstallmentStartDate().isEmpty())){
                    Toast.makeText(CustomerAddActivity.this, "Taksit Başlangıç Tarihini Belirtiniz.", Toast.LENGTH_SHORT).show();
                }else if((customerFinancial.getPaymentMethod() !=null &&  customerFinancial.getPaymentMethod().equals("-1"))&&
                        (customerFinancial.getNumberOfInstallments()==null || customerFinancial.getNumberOfInstallments().isEmpty())){
                    Toast.makeText(CustomerAddActivity.this, "Taksit Sayısını Belirtiniz.", Toast.LENGTH_SHORT).show();
                }else {
                    imgAdd.setClickable(false);
                    Ezrak.getInstance().getCustomer().CustomerAdd(customerContact, customerDeviceModel, customerFinancial, getSharedUserID(),DeviceInformationFragment.getInstance().binding(), new IEzrak() {
                        @Override
                        public void callback(Object data) {
                            switch ((String)data){
                                case "1":
                                    Toast.makeText(CustomerAddActivity.this, "Müşteri Eklendi", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(CustomerAddActivity.this,MainActivity.class));
                                    CustomerAddActivity.this.finish();
                                    imgAdd.setClickable(false);
                                    break;
                                case "-1":
                                    imgAdd.setClickable(false);
                                    Toast.makeText(CustomerAddActivity.this, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                                    //startActivity(new Intent(CustomerAddActivity.this,MainActivity.class));
                                    // CustomerAddActivity.this.finish();
                                    break;
                            }
                        }
                    });

                }

            }
        });
    }

    private void init() {
        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        final TabLayout tabLayout = findViewById(R.id.tabLayout);
        TabItem tabChats = findViewById(R.id.tabChats);
        TabItem tabStatus = findViewById(R.id.tabStatus);
        TabItem tabCalls = findViewById(R.id.tabCalls);
        final ViewPager viewPager = findViewById(R.id.viewPager);

        ///style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
        //
        Red.Instance.getView().TabLayout.PageAdapter(tabLayout,viewPager,getSupportFragmentManager(),tabLayout.getTabCount()
        ).AddFragment(new ContactFragment(),new DeviceInformationFragment(),new FinancialFragment() );

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(CustomerAddActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }



}