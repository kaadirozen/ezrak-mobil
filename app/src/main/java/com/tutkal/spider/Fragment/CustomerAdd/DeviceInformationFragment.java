package com.tutkal.spider.Fragment.CustomerAdd;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.tutkal.spider.Red.Red;
import com.tutkal.spider.databinding.FragmentAddDeviceInformationBinding;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.Customer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DeviceInformationFragment extends Fragment {

    public DeviceInformationFragment() {
        instance=this;
    }
    public static DeviceInformationFragment getInstance() {
        return instance;
    }
    private static DeviceInformationFragment instance;
    private FragmentAddDeviceInformationBinding binding;
    private Customer customer=new Customer();
    private final Calendar myCalendar = Calendar.getInstance();
    private final Calendar myCalendar1 = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date,date1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view=inflater.inflate(R.layout.fragment_add_device_information, container, false);
        binding=FragmentAddDeviceInformationBinding.inflate(inflater,container,false);

        binding.editText9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        binding.editText10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),date1, myCalendar1
                        .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                        myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tarihIslemleri();
        /*customer.setMikron5(binding.FragmentCustomerAddContactCheckBox5Mikorn.isChecked());
        customer.setBlokKarbon(binding.FragmentCustomerAddContactCheckBoxBlokKarbon.isChecked());
        customer.setSonKarbon(binding.FragmentCustomerAddContactCheckBoxSonKarbon.isChecked());
        customer.setKarbonFiltre(binding.FragmentCustomerAddContactCheckBoxKarbonFiltre.isChecked());
        customer.setMebrahFiltre(binding.FragmentCustomerAddContactCheckBoxMebrahFiltre.isChecked());*/

        binding.setCustomerUpdate(customer);
        return binding.getRoot();
    }

    public Customer getCustomerAddDeviceInformation(){
        return customer;
    }
    public FragmentAddDeviceInformationBinding binding(){
        return binding;
    }
    private void tarihIslemleri () {
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                DeviceInformationFragment.this.myCalendar.set(Calendar.YEAR, year);
                DeviceInformationFragment.this.myCalendar.set(Calendar.MONTH, monthOfYear);
                DeviceInformationFragment.this.myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                binding.editText9.setText(sdf.format(myCalendar.getTime()));
            }
        };
        date1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                //myCalendar.set(Calendar.YEAR, year);
                DeviceInformationFragment.this.myCalendar1.set(Calendar.YEAR, year);
                DeviceInformationFragment.this.myCalendar1.set(Calendar.MONTH, monthOfYear);
                DeviceInformationFragment.this.myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                binding.editText10.setText(sdf.format(myCalendar1.getTime()));
            }
        };

    }


}
