package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.tutkal.spider.R;

public class MaintenanceStatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_statistics);

        init();
    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }
}
