package com.tutkal.spider.ezrak.database.Statistics;

abstract class AStatistics {

    public abstract void YearStatistic();

    public abstract void MonthlyStatistic();

    public abstract void WeeklyStatistic();
}
