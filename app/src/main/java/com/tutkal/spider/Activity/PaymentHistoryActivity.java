package com.tutkal.spider.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.furkanbahadirozdemir.fountain.Fountain;
import com.furkanbahadirozdemir.fountain.Interface.IListViewClick;
import com.furkanbahadirozdemir.fountain.Interface.IListview;
import com.furkanbahadirozdemir.fountain.Interface.IRecyclerView;
import com.furkanbahadirozdemir.fountain.List.ListView.FountainList;
import com.furkanbahadirozdemir.fountain.List.RecyclerView.FountainRecyclerView;
import com.google.android.material.snackbar.Snackbar;
import com.tutkal.spider.CDate;
import com.tutkal.spider.Fragment.CustomerInformation.FinancialFragment;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Adapter.RecyclerViewAdapter;
import com.tutkal.spider.Red.Adapter.RecyclerViewOnClick;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PaymentHistoryActivity extends AppCompatActivity {

    private ArrayList<RecyclerClass> arrayList=new ArrayList<>();
    private ArrayList<CDate> arrayListTaksit=new ArrayList<>();
    private RecyclerView recyclerView;
    final RecyclerViews k = Red.Instance.getView().GetRecyclerViews();
    private int paymentMethod;
     FountainRecyclerView view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);
        init();
        //https://www.journaldev.com/23164/android-recyclerview-swipe-to-delete-undo
        //enableSwipeToDeleteAndUndo();
        ImageView imgEdit=findViewById(R.id.imageView);
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });



        getlist();
    }

    private void getlist() {

        if(FinancialFragment.getInstance().getPaymentMethod()){
            k.addItem(5,R.id.textView25,R.id.textView145,R.id.textView148,R.id.textView149,R.id.textView142)
                    .RecyclerView(recyclerView, arrayList, R.layout.item_financial_cash, new RecyclerViewOnClick() {
                        @Override
                        public void OnClick(int ID) {
                        }
                    }).Creat();
        }
        //TAKSİT
        else{
           /* k.addItem(6,R.id.textView63,R.id.textView64,R.id.textView65,R.id.textView66,R.id.textView67,R.id.textView58)
                    .RecyclerView(recyclerView, arrayList, R.layout.item_financial_installment, new RecyclerViewOnClick() {
                        @Override
                        public void OnClick(final int ID) {
                            final String id=String.valueOf(ID);
                            String mesaj="",title="";
                            for(RecyclerClass item : arrayList){
                                if(item.getID()==ID){
                                    if (item.getAs()[5].equals("1")) {
                                        title="Taksit Ödeme";
                                        mesaj="Taksidi Ödemek İstediğinize Eminmisiniz?";
                                    }else if(item.getAs()[5].equals("0")){
                                        title="Taksit İptal Etme";
                                        mesaj="Taksidi İptal Etmek İstediğinize Eminmisiniz?";
                                    }
                                    break;
                                }
                            }

                            Red.Instance.getView().AlertDiyalogTrueFalse(PaymentHistoryActivity.this, title, mesaj, new IEzrak() {
                                @Override
                                public void callback(Object data) {
                                    if((boolean)data){
                                        Alagan.Instance.dbString
                                                .put("command","payInstallment")
                                                .put("payID",id)
                                                .read(new AlaganStringDatabase.AlaganListener() {
                                                    @Override
                                                    public void onResponse(String response) {

                                                    }
                                                });
                                    }
                                }
                            });

                        }
                    }).Creat();*/
            taksit();
        }

        Alagan.Instance.dbString
                .put("command","payDetail")
                .put("customerID",String.valueOf(getSharedCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            /* JSONObject obj = new JSONObject(response);
                            String paymentType = obj.getString("payment_type");


                            if(paymentType.equals("cash")){
                                paymentMethod=0;
                            }else if(paymentType.equals("installment")){
                                paymentMethod=1;
                            }
                            JSONArray detail = obj.getJSONArray("detail");
                            int isUcreti=0;
                            int kalanTutar=0;
                            int t=0;
                            for (int i = 0; i<detail.length();i++)
                            {
                                JSONObject objs = detail.getJSONObject(i);
                                if(i==0 && paymentMethod==0){
                                    isUcreti=objs.getInt("tk_data");
                                    kalanTutar=objs.getInt("tk_data");
                                }
                                else {
                                if(paymentType.equals("cash")){
                                    kalanTutar-=objs.getInt("tk_data");
                                    arrayList.add(new RecyclerClass(2,""+isUcreti,objs.getString("tk_data"),""+kalanTutar,objs.getString("tk_date")));
                                }else if(paymentType.equals("tk_type")){

                                        JSONObject objst = detail.getJSONObject(i);
                                        String a="";
                                        Log.e( "asdadasdasd: ",objst.getString("tk_ok") );

                                            a="Ödenmedi";
                                            if(objst.getString("tk_ok").equals("1")){
                                            a=objst.getString("date");
                                        }
                                        String[] date=objst.getString("tk_date").split("-");
                                        date[1]=getAy(Integer.parseInt(date[1]));
                                        arrayList.add(new RecyclerClass(objst.getInt("tk_id"),date[2],date[1],date[0],objst.getString("tk_data")+" TL",a,objst.getString("tk_ok")));


                                }
                                }
                            }*/
                            JSONObject obj = new JSONObject(response);

                            if(obj.getString("payment_type").equals("installment")){
                                JSONArray array=new JSONArray(obj.getString("detail"));
                                Log.d("DENEME",array.toString());
                                Log.d("DENEME",array.length()+"");
                                for (int j = 0; j <array.length() ; j++) {
                                    JSONObject objst = array.getJSONObject(j);
                                    String taksit=objst.getString("tk_type");
                                    Log.d( "taksit "+j,taksit );
                                    if(taksit.trim().equals("taksit")){

                                        String a="";
                                        a="Ödenmedi";
                                        if(objst.getString("tk_ok").equals("1")){
                                            a=objst.getString("tk_date");
                                        }
                                        String[] date=objst.getString("tk_date").split("-");
                                        date[1]=getAy(Integer.parseInt(date[1]));
                                        CDate cDate=new CDate();
                                        cDate.id=objst.getString("tk_id");
                                         cDate.ay=date[1];
                                        cDate.gun=date[2];
                                        cDate.yil=date[0];
                                        cDate.durum=a;
                                        cDate.ok=objst.getString("tk_ok");
                                        cDate.taksitTL=objst.getString("tk_data")+" Tl";
                                        arrayListTaksit.add(cDate);


                                    }
                                }
                            }else {

                                JSONArray detail = obj.getJSONArray("detail");
                                int isUcreti=0;
                                int kalanTutar=0;
                                int t=0;

                                for (int i = 0; i<detail.length();i++)
                                {
                                    JSONObject objs = detail.getJSONObject(i);

                                   if(objs.getString("tk_type").equals("is_ucreti"))
                                    {
                                        isUcreti = objs.getInt("tk_data");
                                        kalanTutar = objs.getInt("tk_data");
                                    }else{
                                       kalanTutar-=objs.getInt("tk_data");
                                       arrayList.add(new RecyclerClass(2,""+isUcreti,objs.getString("tk_data"),""+kalanTutar,objs.getString("tk_date")));

                                   }


                                }
                            }
                        }
                        catch (Exception e){
                            Log.e("EXECEPTION",e.getMessage());
                        }

                        if(FinancialFragment.getInstance().getPaymentMethod()){
                            k.recyclerViewAdapter.notifyDataSetChanged();
                            Log.e( "asdadasdasd: ","adasd" );
                        }else{
                            view.notifyDataSetChanged();
                            Log.e( "asdadasddasd: ","view" );
                        }
                    }
                });


    }

    private void taksit() {
        view = Fountain.Initialize(PaymentHistoryActivity.this).List().RecyclerView();
        view.SetContext(PaymentHistoryActivity.this);
        view.SetItemList(arrayListTaksit);
        view.SetLayout(R.layout.item_financial_installment);
        final int[] counter = {1};
        view.SetListener(new IRecyclerView() {
            @Override
            public void OnView(View view, Object obj) {
                final CDate mClass=(CDate) obj;

                final TextView txtCounter=view.findViewById(R.id.textView58);
                final TextView txtTaksit=view.findViewById(R.id.textView95);

                TextView txtGun=view.findViewById(R.id.textView63);
                TextView txtAy=view.findViewById(R.id.textView64);
                TextView txtYil=view.findViewById(R.id.textView65);
                TextView txtTaksitMiktari=view.findViewById(R.id.textView66);
                TextView txtTarih=view.findViewById(R.id.textView67);
                final Button button=view.findViewById(R.id.button17);
                txtCounter.setText(""+ counter[0]);

                txtGun.setText(mClass.gun);
                txtAy.setText(mClass.ay);
                txtYil.setText(mClass.yil);
                txtTaksitMiktari.setText(mClass.taksitTL);
                txtTarih.setText(mClass.durum);
                counter[0] = counter[0] +1;

                if(mClass.ok.equals("1")){
                    txtCounter.setTextColor(Color.GREEN);
                    txtTaksit.setTextColor(Color.GREEN);
                    button.setText("Ödemeyi İptal Et");
                }else {
                    txtCounter.setTextColor(Color.RED);
                    txtTaksit.setTextColor(Color.RED);
                    button.setText("Ödeme Yap");
                }

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        Red.Instance.getView().AlertDiyalogTrueFalse(PaymentHistoryActivity.this, "Taksit Ödeme", "Bu İşlemi Onaylıyormusunuz", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Alagan.Instance.dbString
                                            .put("command", "payInstallment")
                                            .put("payID", String.valueOf(mClass.id))
                                            .read(new AlaganStringDatabase.AlaganListener() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Log.e("onClick: ", response);
                                                    //Toast.makeText(PaymentHistoryActivity.this, ""+ String.valueOf(mClass.getID()), Toast.LENGTH_SHORT).show();
                                          /*   if(array[5].equals("1")){
                                            array[5] = "0";
                                            txtCounter.setTextColor(Color.RED);
                                            txtTaksit.setTextColor(Color.RED);
                                            mClass.setAs(array);
                                            button.setText("Ödeme Yap");
                                        }else  if(array[5].equals("0")){
                                            array[5] = "1";
                                            mClass.setAs(array);
                                            button.setText("Ödemeyi İptal");
                                            txtCounter.setTextColor(Color.GREEN);
                                            txtTaksit.setTextColor(Color.GREEN);

                                        }

                                      */
                                                    arrayListTaksit.clear();
                                                    getlist();

                                                }
                                            });
                                }

                            }
                        });




                    }
                });




                    }


            @Override
            public void OnViewHolder(View view) {

            }
        });
        view.Create(recyclerView);
        RecyclerView.LayoutManager mLayoutmanager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutmanager);

    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        recyclerView=findViewById(R.id.Activity_PaymentHistory_RecyclerView);
    }

    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(getApplicationContext(), "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }
    private String getAy(int id){
        String[] aylar={"Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"};
        return aylar[id-1];
    }
}
