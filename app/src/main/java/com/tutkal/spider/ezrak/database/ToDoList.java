package com.tutkal.spider.ezrak.database;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;

import com.tutkal.spider.R;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.View.RecyclerViews;
import com.tutkal.spider.databinding.PopupInformationTodoListBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.widget.AlertDialogLayout;
import androidx.databinding.DataBindingUtil;

public class ToDoList {

    private String toDoListName,toDoListDate,toDoListNote;
    private int userID,toDoListID;
    private ToDoList toDoList;

    public ToDoList(){
    }

    public ToDoList(Context mContext, ToDoList toDoList) {
        this.toDoList=toDoList;
    }

    public ArrayList<?> GetToDoList(final RecyclerViews k){
      final   ArrayList<RecyclerClass> arrayList=new ArrayList<>();
        Alagan.Instance.dbString
                .put("command","toDoList")
                .put("userID",String.valueOf(toDoList.getUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i<jsonArray.length();i++)
                            {
                                JSONObject objs = jsonArray.getJSONObject(i);
                                arrayList.add(new RecyclerClass(objs.getInt("id"),objs.getString("data"),objs.getString("date")));

                            }
                            k.recyclerViewAdapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }

                    }
                });

        return arrayList;
    }

    public void GetMainTodoList(){


    }

    public void ToDoListInformation(final PopupInformationTodoListBinding binding){

        Alagan.Instance.dbString
                .put("command","toDoDetail")
                .put("id",String.valueOf(toDoList.getToDoListID()))
                .read(new AlaganStringDatabase.AlaganListener() {
            @Override
            public void onResponse(String response) {
                JSONObject objs = null;
                try {
                    objs = new JSONObject(response);
                    binding.PopupInformationToDoListData.setText(objs.getString("data"));
                    binding.PopupInformationToDoListDate.setText(objs.getString("date"));
                    binding.PopupInformationToDoListNote.setText(objs.getString("note"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ToDoListAdd (final IEzrak IEzrak){
        //toDoList.setToDoListName(toDoList.getToDoListName());
        Alagan.Instance.dbString
                .put("command","toDoInsert")
                .put("userID",String.valueOf(toDoList.getUserID()))
                .put("data",toDoList.getToDoListName())
                .put("date",toDoList.getToDoListDate())
                .put("note",toDoList.getToDoListNote())
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject objs = null;
                        try {
                            objs = new JSONObject(response);
                            IEzrak.callback(objs.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });


    }

    public void ToDoListUpdate(final IEzrak IEzrak){
        Alagan.Instance.dbString
                .put("command","toDoUpdate")
                .put("id",String.valueOf(toDoList.getToDoListID()))
                .put("data",toDoList.getToDoListName())
                .put("date",toDoList.getToDoListDate())
                .put("note",toDoList.getToDoListNote())
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject objs = null;
                        try {
                            objs = new JSONObject(response);
                            IEzrak.callback(objs.getString("response"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

    }

    public void ToDoListDelete(){
    }


    //Getter Setter
    public String getToDoListName() {
        return toDoListName;
    }
    public String getToDoListDate() {
        return toDoListDate;
    }
    public String getToDoListNote() {
        return toDoListNote;
    }
    public void setToDoListName(String toDoListName) {
        this.toDoListName = toDoListName;
    }
    public void setToDoListDate(String toDoListDate) {
        this.toDoListDate = toDoListDate;
    }
    public void setToDoListNote(String toDoListNote) {
        this.toDoListNote = toDoListNote;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public void setToDoListID(int toDoListID) {
        this.toDoListID = toDoListID;
    }
    public int getUserID() {
        return userID;
    }
    public int getToDoListID() {
        return toDoListID;
    }


}
