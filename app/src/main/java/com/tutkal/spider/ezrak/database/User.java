package com.tutkal.spider.ezrak.database;

import android.content.Context;
import android.util.Log;

import com.tutkal.spider.databinding.ActivityProfilBinding;
import com.tutkal.spider.databinding.ActivityProfilUpdateBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class User  {

    private String userName,password,passwordAgain,nameSurname,storeName,phone1,phone2,phone3,totalMoney,monthlyMoney,programEndTime,customerCount;
    private int userID;
    private User user;

    public User() {
    }

    public User(Context mContext, User user) {
        this.user = user;
    }

    public void LoginControl(final IEzrak IEzrak){
        Alagan.Instance.dbString.put("command","userLogin")
                .put("username",user.getUserName())
                .put("password",user.getPassword())
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e( "onResponse: ",response );
                        IEzrak.callback(response);
                    }
                });
    }

    public void UserProfil(final ActivityProfilUpdateBinding binding){
        Alagan.Instance.dbString
                .put("command","userGetProfile")
                .put("userID",String.valueOf(user.getUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject objs = null;
                        try {
                            objs = new JSONObject(response);
                            binding.ActivityProfilInputEditTextUsername.setText(objs.getString("username"));
                            binding.ActivityProfilInputEditTextPassword.setText(objs.getString("password"));
                            binding.ActivityProfilInputEditTextPasswordAgain.setText(objs.getString("password"));
                            binding.ActivityProfilInputEditTextNamesurname.setText(objs.getString("namesurname"));
                            binding.ActivityProfilInputEditTextStoryname.setText(objs.getString("storename"));
                            binding.ActivityProfilInputEditTextPhone1.setText(objs.getString("phone1"));
                            binding.ActivityProfilInputEditTextPhone2.setText(objs.getString("phone2"));
                            binding.ActivityProfilInputEditTextPhone3.setText(objs.getString("phone3"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


    }

    public void ProfilUpdate(final Context context,final IEzrak IEzrak ){
        String password=user.getPassword();
        String passWordAgain=user.getPasswordAgain();
        if(password.trim().equals(passWordAgain.trim())){
            Alagan.Instance.dbString
                    .put("command","userUpdate")
                    .put("userID",String.valueOf(user.getUserID()))
                    .put("username",user.getUserName())
                    .put("password",user.getPassword())
                    .put("namesurname",user.getNameSurname())
                    .put("storename",user.getStoreName())
                    .put("phone1",user.getPhone1())
                    .put("phone2",user.getPhone2())
                    .put("phone3",user.getPhone3()).read(new AlaganStringDatabase.AlaganListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject objs = new JSONObject(response);
                        IEzrak.callback(objs.getString("response"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else {
            IEzrak.callback("0");
        }
    }
    public void UserProfilDetail(final ActivityProfilBinding binding){
        Alagan.Instance.dbString
                .put("command","userDetail")
                .put("userID",String.valueOf(user.getUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objs = new JSONObject(response);
                    binding.ActivityProfilTextViewNameSurname.setText(objs.getString("namesurname"));
                    binding.ActivityProfilTextViewStoreName.setText(objs.getString("storename"));
                    binding.ActivityProfilTextViewPhone1.setText(objs.getString("phone1"));
                    binding.ActivityProfilTextViewPhone2.setText(objs.getString("phone2"));
                    binding.ActivityProfilTextViewPhone3.setText(objs.getString("phone3"));
                    binding.ActivityProfilTextViewCustomerCount.setText(objs.getString("customer_count"));
                    binding.ActivityProfilTextViewPhoneProgramFinish.setText(objs.getString("end_time"));
                    binding.ActivityProfilTextViewTumPara.setText(objs.getString("all_revenue"));
                    binding.ActivityProfilTextViewBorcPara.setText(objs.getString("all_debt"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    //Getter and Setter
    public String getUserName() {
        return userName;
    }
    public String getPassword() {
        return password;
    }
    public String getPasswordAgain() {
        return passwordAgain;
    }
    public String getNameSurname() {
        return nameSurname;
    }
    public String getStoreName() {
        return storeName;
    }
    public String getPhone1() {
        return phone1;
    }
    public String getPhone2() {
        return phone2;
    }
    public String getPhone3() {
        return phone3;
    }
    public String getTotalMoney() {
        return totalMoney;
    }
    public String getMonthlyMoney() {
        return monthlyMoney;
    }
    public String getProgramEndTime() {
        return programEndTime;
    }
    public String getCustomerCount() {
        return customerCount;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }
    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }
    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }
    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }
    public void setMonthlyMoney(String monthlyMoney) {
        this.monthlyMoney = monthlyMoney;
    }
    public void setProgramEndTime(String programEndTime) {
        this.programEndTime = programEndTime;
    }
    public void setCustomerCount(String customerCount) {
        this.customerCount = customerCount;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return user;
    }
    public int getUserID() {
        return userID;
    }
}
