package com.tutkal.spider.Red;

import android.content.Context;

import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.Red.View.View;

public class Red {

    private Context mContext;

    public static Red Instance;
    private View view;
    private SharedPreferences Shared;

    public Red(Context context) {
        this.mContext = context;
        Instance=this;
        view=getView();
        Shared=getShared();

    }

    public View getView() {
        return new View(mContext);
    }

    public SharedPreferences getShared() {
        return new SharedPreferences(mContext);
    }
}
