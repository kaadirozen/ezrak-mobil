package com.tutkal.spider.ezrak;

import android.content.Context;

import com.tutkal.spider.ezrak.database.Customer;
import com.tutkal.spider.ezrak.database.CustomerList;

import com.tutkal.spider.ezrak.database.Statistics.Statistics;
import com.tutkal.spider.ezrak.database.ToDoList;
import com.tutkal.spider.ezrak.database.User;

public class Ezrak {


    private Context mContext;
    private static Ezrak Instance;
    private ToDoList toDoList;
    private User userrr;
    private Statistics statistics;
    private Customer customer;
    private CustomerList customerList;


    public Ezrak(Context context) {
        this.mContext=context;
        Instance=this;

    }

    public static Ezrak getInstance( ) {

        return Instance;
    }

    public  ToDoList getToDoList(ToDoList toDoList) {
        return new ToDoList(mContext,toDoList);
    }

    public User getUser(User userrr) {
        return new User(mContext, userrr);
    }

    public Statistics getStatistics() {
        return new Statistics(mContext);
    }

    public Customer getCustomer() {
        return new Customer(mContext);
    }

    public CustomerList getCustomerList() {
        return new CustomerList(mContext);
    }
}
