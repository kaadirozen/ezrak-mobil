package com.tutkal.spider.ezrak.database;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tutkal.spider.databinding.FragmentAddDeviceInformationBinding;
import com.tutkal.spider.databinding.FragmentInformationContactBinding;
import com.tutkal.spider.databinding.FragmentInformationDeviceInfarmationBinding;
import com.tutkal.spider.databinding.FragmentInformationFinancialBinding;
import com.tutkal.spider.databinding.PopupCustomerUpdateDeviceBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONException;
import org.json.JSONObject;

public class Customer {

    private int userID,customerID;
    private String nameSurname,phone,province,district,neigborhood,address,deviceModel,odemeYap;
    private String transaction,assemblyDate,dateOfMAintenance;
    private String paymentMethod,workFee, paymentAmount,installmentStartDate,numberOfInstallments,kalanGun,kalanUcret,taksideBolunenUcret;
    boolean sonKarbon,karbonFiltre,blokKarbon,mebrahFiltre,Mikron5;
    private String odemeAdi;


    private int UserID,CustomerID;


    public Customer(Context mContext) {
    }

    public Customer() {

    }

    public  void CustomerAdd(Customer customerContact, Customer customerDeviceModel, Customer customerFinancial, int sharedUserID, FragmentAddDeviceInformationBinding binding, final IEzrak IEzrak) {

        customerDeviceModel.setMikron5(binding.FragmentCustomerAddContactCheckBox5Mikorn.isChecked());
        customerDeviceModel.setBlokKarbon(binding.FragmentCustomerAddContactCheckBoxBlokKarbon.isChecked());
        customerDeviceModel.setSonKarbon(binding.FragmentCustomerAddContactCheckBoxSonKarbon.isChecked());
        customerDeviceModel.setKarbonFiltre(binding.FragmentCustomerAddContactCheckBoxKarbonFiltre.isChecked());
        customerDeviceModel.setMebrahFiltre(binding.FragmentCustomerAddContactCheckBoxMebrahFiltre.isChecked());
//        Log.e( "CustomerAdd: ",customerFinancial.getPaymentAmount() );
        Alagan.Instance.dbString
                .put("command","customerInsert")
                .put("userID",String.valueOf(sharedUserID))
                .put("namesurname",customerContact.getNameSurname().toUpperCase())
                .put("phone",customerContact.getPhone())
                .put("province",customerContact.getProvince())
                .put("district",customerContact.getDistrict())
                .put("neighborhood",customerContact.getNeigborhood())
                .put("address",customerContact.getAddress())
                .put("device_model",customerDeviceModel.getDeviceModel())
                .put("transaction",customerDeviceModel.getTransaction())
                .put("maintenance_date",customerDeviceModel.getDateOfMAintenance())
                .put("assembly_date",customerDeviceModel.getAssemblyDate())
                .put("device_detail",control(customerDeviceModel.isMikron5())
                        +"-"+control(customerDeviceModel.isBlokKarbon())
                        +"-"+control(customerDeviceModel.isSonKarbon())
                        +"-"+control(customerDeviceModel.isKarbonFiltre())
                        +"-"+control(customerDeviceModel.isMebrahFiltre()))
                .put("total_debt",customerFinancial.getWorkFee())
                .put("debt_name",customerFinancial.getOdemeAdi())
                .put("paid_amount",customerFinancial.getPaymentAmount())
                .put("installment_date",customerFinancial.getInstallmentStartDate())
                .put("installments_count",customerFinancial.getNumberOfInstallments())
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject ojbs=new JSONObject(response);
                            IEzrak.callback(ojbs.getString("response"));

                        }catch (Exception e){
                            Log.e("kadir","sdasdasdasdasd");
                        }

                    }
                });



    }


    public  void  CustomerInFormationConctant(final Customer customer, final FragmentInformationContactBinding binding){
        Alagan.Instance.dbString
                .put("command","customerDetailPersonnel")
                .put("id",String.valueOf(customer.getCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject objs=new JSONObject(response);
                            binding.FragmentCustomerInformationContactNameSurname.setText(objs.getString("namesurname"));
                            binding.FragmentCustomerInformationContactPhone.setText(objs.getString("phone"));
                            binding.FragmentCustomerInformationContactProvince.setText(objs.getString("province"));
                            binding.FragmentCustomerInformationContactDiscrict.setText(objs.getString("district"));
                            binding.FragmentCustomerInformationContactMahalle.setText(objs.getString("neighborhood"));
                            binding.FragmentCustomerInformationContactAdress.setText(objs.getString("address"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    public void  CustomerInformationDevice(Customer customer, final FragmentInformationDeviceInfarmationBinding binding){
        Alagan.Instance.dbString
                .put("command","customerDetailDevice")
                .put("id",String.valueOf(customer.getCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("device detail",response);
                            JSONObject objs=new JSONObject(response);
                            binding.FragmentCustomerInformationDeviceCihazModeli.setText(objs.getString("device_model"));
                            binding.FragmentCustomerInformationDeviceYapilanislem.setText(objs.getString("transaction"));
                            binding.FragmentCustomerInformationDeviceMontajTarihi.setText(objs.getString("assembly_date"));
                            binding.FragmentCustomerInformationDeviceBakimTarihi.setText(objs.getString("maintenance_date"));
                            binding.FragmentCustomerInformationDeviceKalanGun.setText(objs.getString("kalangun"));

                            String[] parts = objs.getString("device_detail").split("-");
                            binding.FragmentCustomerInformationDeviceCheckBoxMikron5.setChecked(isChecked(parts[0]));
                            binding.FragmentCustomerInformationDeviceCheckBoxBlokKarbon.setChecked(isChecked(parts[1]));
                            binding.FragmentCustomerInformationDeviceCheckBoxSonKarbon.setChecked(isChecked(parts[2]));
                            binding.FragmentCustomerInformationDeviceCheckBoxKarbonFiltre.setChecked(isChecked(parts[3]));
                            binding.FragmentCustomerInformationDeviceCheckBoxMebrahFiltre.setChecked(isChecked(parts[4]));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                });
        /*customer.setDeviceModel("cihaz modeli");
        customer.setTransaction("işlem");
        customer.setAssemblyDate("montaj Tarihi");
        customer.setDateOfMAintenance("bakım tarihi");
        customer.setKalanGun("kalanGun");*/

    }
    public void  CustomerInformationFinancial(Customer customer, final FragmentInformationFinancialBinding binding){
        Alagan.Instance.dbString
                .put("command","customerDetailPayment")
                .put("id",String.valueOf(customer.getCustomerID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("taksitle ödeme",response);
                            JSONObject objs=new JSONObject(response);
                            if (objs.getString("payment_type").equals("cash")){

                                binding.textView44.setText("Peşin Ödeme");
                                binding.FragmentCustomerInformationFinancialIsUcrreti.setText(objs.getString("is_ucreti"));
                                binding.FragmentCustomerInformationFinancialAlinanUcret.setText(objs.getString("total_amount"));
                                binding.FragmentCustomerInformationFinancialOdemeGecmisi.setText("Ödeme Geçmişi");
                                //binding.textView101.setText(objs.getString("odemeAdi"));
                                int isUcret=objs.getInt("is_ucreti");
                                int odenenUcret=objs.getInt("total_amount");
                                int sonuc=isUcret-odenenUcret;
                                binding.FragmentCustomerInformationFinancialKalanUcret.setText(""+sonuc);
                            }else if(objs.getString("payment_type").equals("installment")){
                                Log.e("asdad",response);
                                binding.FragmentCustomerInformationCashConst.setVisibility(View.VISIBLE);
                                binding.textView44.setText("Taksitle Ödeme");
                                binding.FragmentCustomerInformationFinancialIsUcrreti.setText(objs.getString("is_ucreti"));
                                binding.FragmentCustomerInformationFinancialAlinanUcret.setText(objs.getString("alinan"));
                                binding.FragmentCustomerInformationFinancialOdemeGecmisi.setText("Taksitler");
                                double isUcret=objs.getDouble("is_ucreti");
                                double odenenUcret=objs.getDouble("alinan");
                                double sonuc=isUcret-odenenUcret;
                                binding.FragmentCustomerInformationFinancialKalanUcret.setText(""+sonuc);
                                binding.FragmentCustomerInformationFinancialTaksideBolunenUcret.setText(objs.getString("taksite_bolunen_ucret"));
                                binding.FragmentCustomerInformationFinancialTaksitSayisi.setText(objs.getString("installment_count"));
                                //binding.textView101.setText(objs.getString("odemeAdi"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

    }


    public  void  CustomerUpdateConctant(final Customer customer){
        Alagan.Instance.dbString
                .put("command","customerUpdatePersonnel")
                .put("id",String.valueOf(customer.getCustomerID()))
                .put("namesurname",customer.getNameSurname().toUpperCase())
                .put("phone",customer.getPhone())
                .put("province",customer.getProvince())
                .put("district",customer.getDistrict())
                .put("neighborhood",customer.getNeigborhood())
                .put("address",customer.getAddress()).read(new AlaganStringDatabase.AlaganListener() {
            @Override
            public void onResponse(String response) {

            }
        });
    }
    public  void CustomerUpdateDevice(final Customer customer, PopupCustomerUpdateDeviceBinding binding){
        Alagan.Instance.dbString
                .put("command","customerUpdatePersonnel")
                .put("id",String.valueOf(customer.getCustomerID()))
                .put("device_model",customer.getDeviceModel())
                .put("transaction",customer.getTransaction())
                .put("assembly_date",customer.getAssemblyDate())
                .put("maintenance_date",customer.getDateOfMAintenance())
                .put("device_detail",control(customer.isMikron5())
                        +"-"+control(customer.isBlokKarbon())
                        +"-"+control(customer.isSonKarbon())
                        +"-"+control(customer.isKarbonFiltre())
                        +"-"+control(customer.isMebrahFiltre()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("kadir",customer.getCustomerID()+"----"+response);
                    }
                });


    }

    public void BakimYap(Customer customer, PopupCustomerUpdateDeviceBinding binding) {
        Log.e( "onResponsetarih: ",customer.getDateOfMAintenance() );
        Alagan.Instance.dbString
                .put("command","customerMakeMaintenance")
                .put("id",String.valueOf(customer.getCustomerID()))
                .put("transaction",customer.getTransaction())
                .put("maintenance_date",customer.getDateOfMAintenance())
                .put("device_detail",control(customer.isMikron5())
                        +"-"+control(customer.isBlokKarbon())
                        +"-"+control(customer.isSonKarbon())
                        +"-"+control(customer.isKarbonFiltre())
                        +"-"+control(customer.isMebrahFiltre()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e( "onResponse: ",response );
                    }
                });
    }

    public  void CustomerPaymentInInstallments(){

    }

    public  void CustomerCashPayment(final Customer customer){
        Alagan.Instance.dbString
                .put("command","payDebt")
                .put("userID",String.valueOf(customer.getUserID()))
                .put("customerID",String.valueOf(customer.getCustomerID()))
                .put("amount",customer.getOdemeYap())
                .read(new AlaganStringDatabase.AlaganListener() {
            @Override
            public void onResponse(String response) {
                Log.e("customerid ",String.valueOf(customer.getCustomerID()));
                Log.e("customerid ",String.valueOf(customer.getUserID()) );
                Log.e("respnse ",response );
            }
        });

    }

    private String control(boolean kontrol){
        if (kontrol){
            return "1";
        }else {
            return "0";
        }
    }
    private Boolean isChecked(String data){
        if(data.equals("1")){
            return true;
        }else if(data.equals("0")){
            return false;
        }
        return null;
    }


    public int getUserID() {
        return userID;
    }
    public int getCustomerID() {
        return customerID;
    }
    public String getNameSurname() {
        return nameSurname;
    }
    public String getPhone() {
        return phone;
    }
    public String getProvince() {
        return province;
    }
    public String getDistrict() {
        return district;
    }
    public String getNeigborhood() {
        return neigborhood;
    }
    public String getAddress() {
        return address;
    }
    public String getDeviceModel() {
        return deviceModel;
    }
    public String getTransaction() {
        return transaction;
    }
    public String getAssemblyDate() {
        return assemblyDate;
    }
    public String getDateOfMAintenance() {
        return dateOfMAintenance;
    }
    public String getPaymentMethod() {
        return paymentMethod;
    }
    public String getWorkFee() {
        return workFee;
    }
    public String getPaymentAmount() {
        return paymentAmount;
    }
    public String getInstallmentStartDate() {
        return installmentStartDate;
    }
    public String getNumberOfInstallments() {
        return numberOfInstallments;
    }
    public boolean isSonKarbon() {
        return sonKarbon;
    }
    public boolean isKarbonFiltre() {
        return karbonFiltre;
    }
    public boolean isBlokKarbon() {
        return blokKarbon;
    }
    public boolean isMebrahFiltre() {
        return mebrahFiltre;
    }
    public boolean isMikron5() {
        return Mikron5;
    }
    public void setSonKarbon(boolean sonKarbon) {
        this.sonKarbon = sonKarbon;
    }
    public void setKarbonFiltre(boolean karbonFiltre) {
        this.karbonFiltre = karbonFiltre;
    }
    public void setBlokKarbon(boolean blokKarbon) {
        this.blokKarbon = blokKarbon;
    }
    public void setMebrahFiltre(boolean mebrahFiltre) {
        this.mebrahFiltre = mebrahFiltre;
    }
    public void setMikron5(boolean mikron5) {
        Mikron5 = mikron5;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public void setDistrict(String district) {
        this.district = district;
    }
    public void setNeigborhood(String neigborhood) {
        this.neigborhood = neigborhood;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }
    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
    public void setAssemblyDate(String assemblyDate) {
        this.assemblyDate = assemblyDate;
    }
    public void setDateOfMAintenance(String dateOfMAintenance) {
        this.dateOfMAintenance = dateOfMAintenance;
    }
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    public void setWorkFee(String workFee) {
        this.workFee = workFee;
    }
    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    public void setInstallmentStartDate(String installmentStartDate) {
        this.installmentStartDate = installmentStartDate;
    }
    public void setNumberOfInstallments(String numberOfInstallments) {
        this.numberOfInstallments = numberOfInstallments;
    }

    public String getKalanGun() {
        return kalanGun;
    }

    public void setKalanGun(String kalanGun) {
        this.kalanGun = kalanGun;
    }

    public String getKalanUcret() {
        return kalanUcret;
    }

    public void setKalanUcret(String kalanUcret) {
        this.kalanUcret = kalanUcret;
    }

    public String getTaksideBolunenUcret() {
        return taksideBolunenUcret;
    }

    public void setTaksideBolunenUcret(String taksideBolunenUcret) {
        this.taksideBolunenUcret = taksideBolunenUcret;
    }
    public String getOdemeYap() {
        return odemeYap;
    }

    public void setOdemeYap(String odemeYap) {
        this.odemeYap = odemeYap;
    }


    public String getOdemeAdi() {
        return odemeAdi;
    }

    public void setOdemeAdi(String odemeAdi) {
        this.odemeAdi = odemeAdi;
    }
}
