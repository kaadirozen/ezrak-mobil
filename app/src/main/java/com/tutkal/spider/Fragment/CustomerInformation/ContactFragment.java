package com.tutkal.spider.Fragment.CustomerInformation;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tutkal.spider.Activity.Customer.CustomerInformationActivitiy;
import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;

import com.tutkal.spider.databinding.FragmentInformationContactBinding;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.Ezrak;
import com.tutkal.spider.ezrak.IEzrak;
import com.tutkal.spider.ezrak.database.Customer;


public class ContactFragment extends Fragment {


    public ContactFragment() {
        // Required empty public constructor
        intance=this;
    }
    public static ContactFragment getIntance() {
        return intance;
    }

    private static ContactFragment intance;
    private FragmentInformationContactBinding binding;
    private Customer customer=new Customer();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View view= inflater.inflate(R.layout.fragment_information_contact, container, false);
        binding= FragmentInformationContactBinding.inflate(inflater,container,false);
        customer.setCustomerID(getSharedCustomerID());
        Ezrak.getInstance().getCustomer().CustomerInFormationConctant(customer,binding);
        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Red.Instance.getView().AlertDiyalogTrueFalse(getContext(), "Müşteri Silme",
                        "Müşteriyi Silmek İstediğinize Emin misiniz?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Alagan.Instance.dbString
                                            .put("command","customerDelete")
                                            .put("id",String.valueOf(getSharedCustomerID()))
                                            .read(new AlaganStringDatabase.AlaganListener() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Toast.makeText(getContext(), "Müşteri Silindi.", Toast.LENGTH_SHORT).show();
                                                    getActivity().finish();
                                                }
                                            });
                                }
                            }
                        });
            }
        });
        binding.setCustomerInformation(customer);
        return binding.getRoot();
    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(getContext(), "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }
    private int getSharedCustomerID(){
        final String[] customerID = new String[1];
        Red.Instance.getShared().SharedGet(getContext(), "customerID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                customerID[0] =id;
            }
        });
        return Integer.parseInt(customerID[0]);
    }

    public String Call(){
        return customer.getPhone();
    }
    public Customer getCustomer(){
        return customer;
    }

}
