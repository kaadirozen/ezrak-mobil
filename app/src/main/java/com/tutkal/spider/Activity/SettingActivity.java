package com.tutkal.spider.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.tutkal.spider.Activity.Customer.CustomerListActivity;
import com.tutkal.spider.R;
import com.tutkal.spider.Red.Class.RecyclerClass;
import com.tutkal.spider.Red.Red;
import com.tutkal.spider.Red.View.SharedPreferences;
import com.tutkal.spider.Service.BakimGunuAlarmReceiver;
import com.tutkal.spider.Service.BakimOncesiAlarmReceiver;
import com.tutkal.spider.Service.SmsAlarmReceiver;
import com.tutkal.spider.ezrak.Alagan.Alagan;
import com.tutkal.spider.ezrak.Alagan.Class.AlaganStringDatabase;
import com.tutkal.spider.ezrak.IEzrak;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SettingActivity extends AppCompatActivity {

    private TimePickerDialog timePickerDialog,timePickerDialog2;
    final static int REQUEST_CODE = 1;

    private ImageView imgEdit;
    private Switch otoSms,otoBildirim;
    private TextView bakimGunu,bakimOncesi,sms ,txtBakimGunu,txtBakimOncesi,txtSms;
    private int txtControl=0;


    private String strBakimGunu="",strBakimOncesi="",strSms="",strBakimGunuSer="",strBakimOncesiSer="",strSmsSer="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(SettingActivity.this, ""+otoSms.isChecked(), Toast.LENGTH_SHORT).show();
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtControl=1;
                smsCalander(false);

            }
        });
        bakimGunu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtControl=2;
                smsCalander(false);
            }
        });
        bakimOncesi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtControl=3;
                smsCalander(false);
            }
        });


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(SettingActivity.this, "aa"+strBakimGunuSer, Toast.LENGTH_SHORT).show();




                Red.Instance.getView().AlertDiyalogTrueFalse(SettingActivity.this, "Ayarları Güncelleme",
                        "Ayarları kaydetmek istediğinize eminmisiniz?", new IEzrak() {
                            @Override
                            public void callback(Object data) {
                                if((boolean)data){
                                    Alagan.Instance.dbString
                                            .put("command","settingsSave")
                                            .put("userID",String.valueOf(getSharedUserID()))
                                            .put("autoSMS",control(otoSms.isChecked()))
                                            .put("notification",control(otoBildirim.isChecked()))
                                            .put("maintenanceDay",strBakimGunu)
                                            .put("maintenanceDay1",strBakimGunuSer)
                                            .put("SMSTime",strSms)
                                            .put("SMSTime1",strSmsSer)
                                            .put("remindingMaintenance",strBakimOncesi)
                                            .put("remindingMaintenance1",strBakimOncesiSer)
                                            .read(new AlaganStringDatabase.AlaganListener() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Alagan.Instance.dbString
                                                            .put("command","settingsGet")
                                                            .put("userID",String.valueOf(getSharedUserID()))
                                                            .read(new AlaganStringDatabase.AlaganListener() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    try {


                                                                        JSONObject objs=new JSONObject(response);
                                                                        JSONObject object = objs.getJSONObject("settings");
                                                                        txtBakimGunu.setText("Saati "+object.getString("maintenanceDay"));
                                                                        txtBakimOncesi.setText("Saati "+object.getString("remindingMaintenance"));
                                                                        txtSms.setText("Saati "+object.getString("SMSTime"));
                                                                        otoSms.setChecked(isCheck(object.getString("autoSMS")));
                                                                        otoBildirim.setChecked(isCheck(object.getString("notification")));

                                                                        strSmsSer=object.getString("SMSTime1");
                                                                        strBakimGunuSer=object.getString("maintenanceDay1");
                                                                        strBakimOncesiSer=object.getString("remindingMaintenance1");






                                                                        finish();
                                                                        startActivity(getIntent());

                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }



                                                                }
                                                            });

                                                }
                                            });
                                }
                            }
                        });

            }
        });
        Alagan.Instance.dbString
                .put("command","settingsGet")
                .put("userID",String.valueOf(getSharedUserID()))
                .read(new AlaganStringDatabase.AlaganListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("setting get",response);

                        try {


                            JSONObject objs=new JSONObject(response);
                            JSONObject object = objs.getJSONObject("settings");
                            strBakimGunu=object.getString("maintenanceDay");
                            strBakimOncesi=object.getString("remindingMaintenance");
                            strSms=object.getString("SMSTime");

                            strSmsSer=object.getString("SMSTime1");
                            strBakimGunuSer=object.getString("maintenanceDay1");
                            strBakimOncesiSer=object.getString("remindingMaintenance1");

                            txtBakimGunu.setText("Saat: "+object.getString("maintenanceDay"));
                            txtBakimOncesi.setText("Saat: "+object.getString("remindingMaintenance"));
                            txtSms.setText("Saat: "+object.getString("SMSTime"));
                            otoSms.setChecked(isCheck(object.getString("autoSMS")));
                            otoBildirim.setChecked(isCheck(object.getString("notification")));

                            JSONArray array = new JSONArray(objs.getString("customers"));
                            for (int i = 0; i<array.length();i++)
                            {
                                JSONObject objst = array.getJSONObject(i);
                                Log.e("asdasd",objst.toString());

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                });



        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPickerDialog(false);

            }
        });*/


    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        imgEdit=findViewById(R.id.imageView3);
        otoSms=findViewById(R.id.switch1);

        otoBildirim=findViewById(R.id.switch2);

        bakimGunu=findViewById(R.id.textView33);
        bakimOncesi=findViewById(R.id.textView39);
        sms=findViewById(R.id.textView31);

        txtBakimGunu=findViewById(R.id.textView91);
        txtBakimOncesi=findViewById(R.id.textView94);
        txtSms=findViewById(R.id.textView88);
    }
    private void smsCalander(boolean is24hour) {

        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(
                SettingActivity.this,
                onTimeSetListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                is24hour);
        timePickerDialog.setTitle("Alarm Ayarla");

        timePickerDialog.show();


    }



    TimePickerDialog.OnTimeSetListener onTimeSetListener
            = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();

            calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSet.set(Calendar.MINUTE, minute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if(calSet.compareTo(calNow) <= 0){

                calSet.add(Calendar.DATE, 1);
            }
            switch (txtControl){
                case 1:
                    setAlarm1(calSet);
                    break;
                case 2:
                    setAlarm2(calSet);
                    break;
                case 3:
                    setAlarm3(calSet);
                    break;
            }

        }};


    private void setAlarm1(Calendar alarmCalender){


       // Toast.makeText(getApplicationContext(),"Alarm Ayarlandı!",Toast.LENGTH_SHORT).show();
        Intent intent3 = new Intent(getBaseContext(), SmsAlarmReceiver.class);
        PendingIntent pendingIntent3= PendingIntent.getBroadcast(getBaseContext(), REQUEST_CODE, intent3, 0);
        AlarmManager alarmManager3 = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager3.set(AlarmManager.RTC_WAKEUP, alarmCalender.getTimeInMillis(), pendingIntent3);


        SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("HH:mm");

        strSms = simpleDateFormat.format(alarmCalender.getTime());
        strSmsSer= String.valueOf(alarmCalender.getTimeInMillis());

        txtSms.setText("Saat: "+strSms);
      //  Toast.makeText(this, "aa"+otoSms.isChecked(), Toast.LENGTH_SHORT).show();


    }
    private void setAlarm2(Calendar alarmCalender){



        //Toast.makeText(getApplicationContext(),"Alarm Ayarlandı!",Toast.LENGTH_SHORT).show();
        Intent intent2 = new Intent(getBaseContext(), BakimGunuAlarmReceiver.class);
        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(getBaseContext(), REQUEST_CODE, intent2, 0);
        AlarmManager alarmManager2 = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager2.set(AlarmManager.RTC_WAKEUP, alarmCalender.getTimeInMillis(), pendingIntent2);

        SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("HH:mm");

        strBakimGunu = simpleDateFormat.format(alarmCalender.getTime());
        strBakimGunuSer= String.valueOf(alarmCalender.getTimeInMillis());

        txtBakimGunu.setText("Saat: "+strBakimGunu);


    }
    private void setAlarm3(Calendar alarmCalender){


//        Toast.makeText(getApplicationContext(),"Alarm Ayarlandı!",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getBaseContext(), BakimOncesiAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), REQUEST_CODE, intent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalender.getTimeInMillis(), pendingIntent);

        SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("HH:mm");

        strBakimOncesi = simpleDateFormat.format(alarmCalender.getTime());
        strBakimOncesiSer= String.valueOf(alarmCalender.getTimeInMillis());

        txtBakimOncesi.setText("Saat: "+strBakimOncesi);




    }

    private int getSharedUserID(){
        final String[] userID = new String[1];
        Red.Instance.getShared().SharedGet(SettingActivity.this, "userID", new SharedPreferences.RedSharedListener() {
            @Override
            public void onResponse(String id) {
                userID[0] =id;
            }
        });
        return Integer.parseInt(userID[0]);
    }

    public void ActivitySettingImageClick(View view) {
        switch (view.getId()){
            //geri
            case R.id.imageView12:
                onBackPressed();
                break;
            //ayarla
            case R.id.imageView3:
                break;
        }
    }
    private String control(boolean kontrol){
        if (kontrol){
            return "1";
        }else {
            return "0";
        }
    }

    private boolean isCheck(String kontrol){
        if (kontrol.equals("1")){
            return true;
        }else {
            return false;
        }
    }
}
